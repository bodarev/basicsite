<html><head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <title>Error 404</title>
        
        <style type="text/css" media="screen">
            html, body {
                height: 100%;
                margin: 0;
            }
            body {
                background: #f6f6f6;
                color: #737373;
                font: 18px/1.6em 'museo-sans-rounded',Helvetica,Arial,Verdana,sans-serif;
                height: 100%;
                min-height: 100%;
            }
            @font-face {
                font-family: 'museo-sans-rounded';
                src: url('//d7mj4aqfscim2.cloudfront.net/proxy/fonts/museo/museosansrounded-300-webfont.eot');
                src: url('//d7mj4aqfscim2.cloudfront.net/proxy/fonts/museo/museosansrounded-300-webfont.eot?#iefix') format('embedded-opentype'),
                     url('//d7mj4aqfscim2.cloudfront.net/proxy/fonts/museo/museosansrounded-300-webfont.woff') format('woff'),
                     url('//d7mj4aqfscim2.cloudfront.net/proxy/fonts/museo/museosansrounded-300-webfont.ttf') format('truetype'),
                     url('//d7mj4aqfscim2.cloudfront.net/proxy/fonts/museo/museosansrounded-300-webfont.svg#museo_sans_rounded300') format('svg');
                font-weight: 300;
                font-style: normal;
            }
            @font-face {
                font-family: 'museo-sans-rounded';
                src: url('//d7mj4aqfscim2.cloudfront.net/proxy/fonts/museo/museosansrounded-700-webfont.eot');
                src: url('//d7mj4aqfscim2.cloudfront.net/proxy/fonts/museo/museosansrounded-700-webfont.eot?#iefix') format('embedded-opentype'),
                     url('//d7mj4aqfscim2.cloudfront.net/proxy/fonts/museo/museosansrounded-700-webfont.woff') format('woff'),
                     url('//d7mj4aqfscim2.cloudfront.net/proxy/fonts/museo/museosansrounded-700-webfont.ttf') format('truetype'),
                     url('//d7mj4aqfscim2.cloudfront.net/proxy/fonts/museo/museosansrounded-700-webfont.svg#museo_sans_rounded700') format('svg');
                font-weight: 700;
                font-style: normal;
            }
            img {
                display: block;
            }
            #wrapper {
                height: 100%;
                min-height: 300px;
                min-width: 700px;
                position: relative;
                width: 100%;
            }
            #box_centered {
width: 50%;
  height: 50%;
  overflow: auto;
  margin: auto;
  position: absolute;
  top: 0; left: 0; bottom: 0; right: 0;
                width: 900px;
            }
            .pane-left {
                width: 470px;
                float: left;
            }
            .pane-right {
                width: 430px;
                float: right;
				padding-top: 70px;
            }
            .owl {
                margin: -20px 0 0 0;
            }
            h1 {
                font-size: 36px;
                margin: 0 0 25px 0;
                font-weight: 300;
                width: 400px;
            }
            p {
                margin: 0 0 25px 0;
            }
            a {
                color: #737373;
                text-decoration: none;
                font-weight: bold;
            }
            a:hover {
                text-decoration: underline;
            }
        </style>
    <style type="text/css"></style></head>
        <body><div id="wrapper">
            <div id="box_centered" class="clearfix">
                <div class="pane-left">
                    <img src="<?php echo base_url(); ?>/img/404.png" class="owl">
                </div>
                <div class="pane-right">
                    <p>
                        Sorry, the page you were looking for doesn’t exist. <br>
                        Try going to <a href="<?php echo base_url(); ?>">Basicsite.com</a>.
                    </p>
                </div>
                
                
            </div>    

        </div>
        
        
    

</body></html>