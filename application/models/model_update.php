<?php
	
	class Model_update extends CI_Model {
		
		function update_email_from_cart($ip, $email){
			
            $data = array(
               'email' => $email
            );

            $this->db->where('id_client', $ip);
            $this->db->where('email', '0');
            $this->db->update('cart', $data);  
  				
		}
		
        function update_order($id, $email){		
			
            $data = array(
               'status' => 'canceled'
            );
        
            $this->db->where('id_order', $id);
			$this->db->where('email', $email);
            $this->db->update('orders_total', $data); 			
		}
		
        
		function update_address_user_default($id, $email){		
			
            $data = array(
               'priority' => '0'
            );
            
			$this->db->where('email', $email);
            $this->db->update('users_address', $data); 
            
            $data = array(
               'priority' => '1'
            );
            
            $this->db->where('id', $id);
			$this->db->where('email', $email);
            $this->db->update('users_address', $data); 			
		}
		
		function update_address_user($id, $email, $data){		
			
			$this->db->where('id', $id);
			$this->db->where('email', $email);
			$this->db->update('users_address', $data); 			
		}
		
		function update_password($key_new) {
			$data = array(
				'email' => $this->input->post('email'),
				'password' => $key_new
			);
			$this->db->where('email', $this->input->post('email'));
			$query =  $this->db->update('users', $data);
			
			if($query) {
				return true;
			} else {
				return false;
			}
		}
		
		function update_avatar($email) {
            
            $time = time();
            
		/* Incarcarea fisierului pe server */
			
			$config['upload_path'] = './prod_img/photos/';
			$config['allowed_types'] = 'gif|jpg|png|gpeg';
			$config['max_size']	= '102400';
			$config['encrypt_name']	= TRUE;
			$config['remove_spaces'] = TRUE;
			
			$this->load->library('upload', $config);
			
			//$this->upload->do_upload();
            
            if ( ! $this->upload->do_upload()) {
                
            /* Scoatem avatarul vechi pentruca este eroare la incarcarea celui nou  */
                
                $this->db->select('*');
                $this->db->from('users');
                $this->db->where('email', $email);
                $query = $this->db->get();
                $row = $query->row(); 
                $old_avatar = $row->avatar;
                
                return $old_avatar;
                
            } else {
			
                $data_img = $this->upload->data();

            /* Crearea avatarului */

                $config = array (
                    'image_library' => 'gd2',
                    'source_image' => $data_img['full_path'],
                    'new_image' => APPPATH.'../prod_img/avatar/'.$time.'b12.png',
                    'create_thumb' => FALSE,
                    'maintain_ratio' => FALSE,
                    'width' => 580,
                    'height' => 325
                );

                $this->load->library('image_lib', $config);

                $this->image_lib->resize();


                $avatar = 'prod_img/avatar/'.$time.'b12.png';

            /* Stergem avatarul vechi */ 


                $this->db->select('*');
                $this->db->from('users');
                $this->db->where('email', $email);
                $query = $this->db->get();
                $row = $query->row(); 
                $old_avatar = $row->avatar;

                unlink($old_avatar);

                /* Inseram in tabela users */ 

                 $data = array(
                   'avatar' => $avatar
                );

                $this->db->where('email', $email);
                $this->db->update('users', $data);


            /* Stergem fisierul sursa	*/

                unlink($data_img['full_path']);

                return $avatar; 
            }
		}
		
	}

?>