<?php
	
	class Model_get extends CI_Model {
		function get_all_data($namePage){
			if ($this->session->userdata('langue')) {
				$lang = $this->session->userdata('langue');
			} else {
				$lang = 'Ro';
			}
			$data = 'data'.$lang;
			
			$this->db->select("key, $data as denumire");
			$this->db->from("pagedata");		
			$this->db->where('namePage =', $namePage);	
			$this->db->or_where('namePage =', 'search');
			$this->db->or_where('namePage =', 'simproducts');
			$this->db->or_where('namePage =', 'bag');
			$this->db->or_where('namePage =', 'order');
            $this->db->or_where('namePage =', 'currency');
            
			$this->db->or_where('namePage =', 'nav');
            
			$this->db->or_where('namePage =', 'months');
			$this->db->or_where('namePage =', 'status');
            
			$this->db->or_where('namePage =', 'user')
                ;
            $this->db->or_where('namePage =', 'msg');
			$this->db->or_where('namePage =', 'button');
			$this->db->or_where('namePage =', 'link');
			$this->db->or_where('namePage =', 'validation');
			$this->db->or_where('namePage =', 'prod_categ');	
 			$query = $this->db->get();
	
			$data = array();
			
			foreach ($query->result() as $row)
			{
				foreach ($row as $k=>$value) {   
					if ($k == 'key') {
						$campo = $value;
					} else {
						$data[$campo] = $value;		
					}		
				}
			}
			
			$data['current'] = $namePage;
			$index = $namePage."_title";
			$data['tab_title'] = $data[$index];
			
			return $data;			
		}
		
		function get_news($limit){
            if ($this->session->userdata('langue')) {
				$lang = $this->session->userdata('langue');
			} else {
				$lang = 'Ro';
			}
			$title = 'text'.$lang;
	
			$this->db->select("title, $title as denumire");	
			$this->db->from("news");		
			$this->db->limit(2, $limit);			
 			$query = $this->db->get();
			
			return $query->result();			
		}
		function get_count_from_news(){
			$count = $this->db->count_all('news');	
			
			return $count;			
		}
		
		function get_orders($email){		
            
            $this->db->select('*');
            $this->db->group_by('id_order');
            $this->db->from('orders_total');
			$this->db->where('email', $email);
            
            $this->db->order_by("date", "asc");
       		
 			$query = $this->db->get();

			return $query->result();
		}
        
        function get_order_info($id_order){		
            
            if ($this->session->userdata('langue')) {
				$lang = $this->session->userdata('langue');
			} else {
				$lang = 'Ro';
			}
            $email = $this->session->userdata('email');
            
            $this->db->select('*');
            $this->db->from('orders');
            $this->db->join('prod', 'prod.id = orders.id_prod');
            $this->db->join('det_prod', 'prod.id = det_prod.id_prod');
            $this->db->where('orders.id_order', $id_order);
            $this->db->where('det_prod.lang_prod', $lang);	

            $query = $this->db->get();

            return $query->result();
		}
		
		function get_count_from_cart($ip, $email){
			
			if ($email != '') {
				$this->db->select_sum('quantity');
				$this->db->from("cart");
				$this->db->where('id_client', $ip);
				$this->db->where('email', $email);
				$query = $this->db->get();
				$row = $query->row(); 
				$count = $row->quantity;
				return $count;
			}
							
		}
		
		function get_count_product_from_cart($id, $ip, $email){
			
			$this->db->from("cart");
			$this->db->where('id_prod', $id);
			$this->db->where('id_client', $ip);
			$this->db->where('email', $email);		
			$query = $this->db->get();	
			foreach ($query->result() as $row)
			{
			    return $row->quantity;
			}				
		}  
		
		function get_products($categ, $ip, $email){		
			
			if ($email != '') {
				$this->db->select('*');
				$this->db->from('prod');
				$this->db->join('det_prod', 'prod.id = det_prod.id_prod');
				if ($ip > '') {
					$this->db->join('cart', 'prod.id = cart.id_prod');
					$this->db->where('cart.id_client', $ip);
				}
				if ($email > '') {
					$this->db->where('cart.email', $email);
				}
				if  ($categ > 0) {
					$this->db->where('prod.categ_prod', $categ );	
				}
				if ($this->session->userdata('langue')) {
				$lang = $this->session->userdata('langue');
                } else {
                    $lang = 'Ro';
                }
				
				$this->db->where('det_prod.lang_prod', $lang);	
				
				$query = $this->db->get();
				
				return $query->result();
			}
		}
		
		function get_all_products($categ){		
			
			$this->db->select('*');
			$this->db->from('prod');
			$this->db->join('det_prod', 'prod.id = det_prod.id_prod');
			if  ($categ > 0) {
				$this->db->where('prod.categ_prod', $categ );	
			}
			if ($this->session->userdata('langue')) {
				$lang = $this->session->userdata('langue');
			} else {
				$lang = 'Ro';
			}
			$this->db->where('det_prod.lang_prod', $lang);	
			
			$query = $this->db->get();
			
			return $query->result();
		}
		
		function get_amount(){		
			$this->db->select("denom, amount");	
			$this->db->from("amount");			
 			$query = $this->db->get();
	
			$data = array();
			
			foreach ($query->result() as $row)
			{
				foreach ($row as $k=>$value) {   
					if ($k == 'denom') {
						$campo = $value;
					} else {
						$data[$campo] = $value;		
					}		
				}
			}
			
			return $data;				
		}
        
        function get_restaurant_address($id=''){	
			
            $this->db->select("*");	
			$this->db->from("restaurant_address");
            if ($id != '') {
				$this->db->where('id', $id);
			}
            $this->db->order_by("id", "asc");
 			
			$query = $this->db->get();
	
			return $query->result();
		}
        
        function get_category(){
				
			$this->db->select('*');
			$this->db->from('category');
			$this->db->order_by('name_group', 'desc');
            
 			$query = $this->db->get();
          
			return $query->result();			
		} 
		
		function get_search($match){
			if ($this->session->userdata('langue')) {
				$lang = $this->session->userdata('langue');
			} else {
				$lang = 'Ro';
			}
			$this->db->from("det_prod");
			$this->db->join('prod', 'prod.id = det_prod.id_prod');
			$this->db->where('det_prod.lang_prod', $lang);		
			$this->db->like('det_prod.title_prod', $match);	
		
 			$query = $this->db->get();
			
			return $query->result();			
		}
		
		function get_user_address($email, $id){		
			
			$this->db->select('*');
			$this->db->from('users_address');
			$this->db->where('email', $email);
			if ($id != '') {
				$this->db->where('id', $id);
			}
			$this->db->order_by("priority", "desc");	

 			$query = $this->db->get();
			
			return $query->result();			
		}
        
        function get_user_data($email){		
			
            $this->db->select('*');
			$this->db->from('users');
			$this->db->where('users.email', $email);
            $this->db->where('users_address.priority', '1');
            $this->db->join('users_address', 'users.email = users_address.email');

 			$query = $this->db->get();
			
			return $query->row();			
		}
		
		function is_valid_key($key_conferm) {
			$this->db->where('key', $key_conferm);
			$query = $this->db->get('temp_users');
			
			if($query->num_rows() == 1) {
				return true;
			} else {
				return false;
			}
		}
		
		function can_log_in() {
			$this->db->where('email', $this->input->post('email'));
			$this->db->where('password', md5($this->input->post('password')));
			
			$query = $this->db->get('users');
			
			if ($query->num_rows() == 1) {
				return true;
			} else {
				return false;
			}
		}
		
		function email_exist($key) {
			$this->db->where('email',$key);
			$query = $this->db->get('users');
            
            $count = $query->num_rows();
			return $count;
		}
        
        function restaurant_exist($key) {
			$this->db->where('id',$key);
			$query = $this->db->get('restaurant_address');

			$count = $query->num_rows();
			return $count;
		}
        
        function client_exist($key) {
			$this->db->where('id',$key);
			$query = $this->db->get('users_address');
            
            $count = $query->num_rows();
			return $count;
		}
		
	}

?>