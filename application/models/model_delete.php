<?php
	
	class Model_delete extends CI_Model {
			
		function delete_product_from_cart($id, $ip, $email){
			
			$this->db->where('id_prod', $id);
			$this->db->where('id_client', $ip);
            $this->db->where('email', $email);
			$this->db->delete('cart'); 
  				
		}
		
		function delete_user_address($id, $email){		
			
			$this->db->where('id', $id);
			$this->db->where('email', $email);
			$this->db->delete('users_address'); 			
		}
		
	}

?>