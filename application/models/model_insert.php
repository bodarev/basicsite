<?php
	
	class Model_insert extends CI_Model {
			
		function insert_product_to_cart($data){
			
			$this->db->insert('cart', $data); 				
		}
		
		 function insert_order($data){
			
			$this->db->insert('orders', $data); 				
		}
		
		function insert_total_order($data){
			
			$this->db->insert('orders_total', $data); 				
		}
		
		function insert_user_address($data){
            $email = $data['email'];
            $this->db->where('email',$email);
            $this->db->where('priority', '1');
			$query = $this->db->get('users_address');
            
            $count = $query->num_rows();
            
            if ( $count > 0 ) {
                $priority = 0;
            } else {
                $priority = 1;
            }
            
            $data['priority'] = $priority;
            
			$this->db->insert('users_address', $data); 			
		}
        
		function insert_temp_user($key_conferm) {
			$data = array(
				'email' => $this->input->post('email'),
				'password' => md5($this->input->post('password')),
				'key' => $key_conferm
			);
			$query =  $this->db->insert('temp_users', $data);
			
			if($query) {
				return true;
			} else {
				return false;
			}
		}
		
		function insert_user($key_conferm) {
			$this->db->where('key', $key_conferm);
			$temp_user = $this->db->get('temp_users');
			
			if($temp_user) {
				$row = $temp_user->row();
				
				$data = array (
					'email' => $row->email,
					'password' =>$row->password
				);
				
				$did_insert_user = $this->db->insert('users', $data);
				
				if($did_insert_user) {
					$this->db->where('email', $data['email']);
					$this->db->delete('temp_users');
					return $data['email'];
				} else {
					return false;
				}
			}
		}		
		
	}

?>