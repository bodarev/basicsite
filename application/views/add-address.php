<?php include 'lang.php'; ?>      
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>

<div class="container">
  <div class="col-lg-8 col-md-8">
  	<div class="content"> 			  
    	<? echo heading ($address_title,1);
		
					$name_errore = form_error('name');
					$city_errore = form_error('city');
					$strada_errore = form_error('strada');
					$bloc_errore = form_error('bloc');
					$phone_errore = form_error('phone');
										
						echo '<form action="'.base_url().'user/address_validation/add/click" method="post" accept-charset="utf-8" id="address">        
							  	
								<div id="name-group" class="row">
									<div class="col-sm-12">
										<label>'.$user_name.'*</label><br/>
										<input type="text" name="name" placeholder="'.$user_name.'" value="'.set_value("name").'" id="name">
									</div> 
								</div>
								
								<div id="localitate-group" class="row">
									<div class="col-sm-12">
										<label>'.$user_city.'*</label><br/>
										<input type="text" name="city" placeholder="'.$user_city.'" value="'.set_value("city").'" id="city">
									</div> 	
								</div>	
								
								<div class="row">
									
									<div id="strada-group" class="col-sm-5">
											<label>'.$user_strada.'*</label><br/>
											<input type="text" name="strada" placeholder="'.$user_strada.'" value="'.set_value("strada").'" id="strada">
									</div>
									
									<div id="bloc-group" class="col-sm-4">
											<label>'.$user_bloc.'*</label><br/>
											<input type="text" name="bloc" placeholder="'.$user_bloc.'" value="'.set_value("bloc").'" id="bloc">
									</div>
									
									<div id="apartament-group" class="col-sm-3">
											<label>'.$user_apartament_short.'</label><br/>
											<input type="text" name="apartament" placeholder="'.$user_apartament_short.'" value="'.set_value("apartament").'" id="apartament">
									</div>	 
								</div>
								
								<div class="row">
									<div id="scara-group" class="col-sm-4"> 
										<label>'.$user_entrance.'</label><br/>
										<input type="text" name="scara" placeholder="'.$user_entrance.'" value="'.set_value("scara").'" id="scara">
									</div>
									
									<div id="etaj-group" class="col-sm-4"> 
										<label>'.$user_floor.'</label><br/>
										<input type="text" name="etaj" placeholder="'.$user_floor.'" value="'.set_value("etaj").'" id="etaj">
									</div>
									<div id="interfon-group" class="col-sm-4"> 
											<label>'.$user_interfon.'</label><br/>
											<input type="text" name="interfon" placeholder="'.$user_interfon.'" value="'.set_value("interfon").'" id="interfon">
									</div>	 
								</div>
								
								<div id="phone-group" class="row">
									<div class="col-sm-4">
										<label>'.$user_phone.'*</label><br/>
										<input type="text" name="phone" id="phone" 
                                        value="'.set_value("phone").'" 
                                        pattern="^\+373(\s+)?\(?(79|78|69|68|67|60)\)?(\s+)?[0-9]{3}-?[0-9]{3}$">
									</div>
								</div>
								<br />
								<div class="left" style="width:100%">
									<p><input class="btn btn-success btn-embossed" type="submit" name="signup_submit" value="'.$ButtonAdd.'"></p>			
								</div>
							</form>';
					
			?>
            
            <?php if ($name_errore != '') { ?> 
				<script> $('#name-group input').css({"border-color": "#b94a48", "box-shadow" : "0 0 0 5px #f2dede"}); </script>
			<?php } ?>
            <?php if ($city_errore != '') { ?> 
				<script> $('#localitate-group input').css({"border-color": "#b94a48", "box-shadow" : "0 0 0 5px #f2dede"}); </script>
			<?php } ?>
            <?php if ($strada_errore != '') { ?> 
				<script> $('#strada-group input').css({"border-color": "#b94a48", "box-shadow" : "0 0 0 5px #f2dede"}); </script>
			<?php } ?>
            <?php if ($bloc_errore != '') { ?> 
				<script> $('#bloc-group input').css({"border-color": "#b94a48", "box-shadow" : "0 0 0 5px #f2dede"}); </script>
			<?php } ?>
            <?php if ($phone_errore != '') { ?> 
				<script> $('#phone-group input').css({"border-color": "#b94a48", "box-shadow" : "0 0 0 5px #f2dede"}); </script>
			<?php } ?>
            
    
 </div>
  </div>
  <?php include 'right_box.php'; ?>
</div>

<?php include 'footer.php'; ?>
 <script>	
		$(document).ready(function(){
			$("#name").blur(function(){
				var name = $("#name").val();
				$.post("<?php echo base_url()."user/address_validation/add/dinamic/"?>",
				{
				  name: name
				},
				function(data){
					var obj = $.parseJSON(data);
					$("#name-group label.error").html(obj.name_errore);
					if (obj.name_errore != '') {
						$('#name-group input').css({"border-color": "#b94a48", "box-shadow" : "0 0 0 5px #f2dede"});
					} else {
						$('#name-group input').css({"border-color": "#c7d0d2", "box-shadow" : "inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #f5f7f8"});
					}
				});
			});
			
			$("#city").blur(function(){
				var city = $("#city").val();
				$.post("<?php echo base_url()."user/address_validation/add/dinamic/"?>",
				{
				  city: city
				},
				function(data){
					var obj = $.parseJSON(data);
					$("#localitate-group label.error").html(obj.city_errore);
					if (obj.city_errore != '') {
						$('#localitate-group input').css({"border-color": "#b94a48", "box-shadow" : "0 0 0 5px #f2dede"});
					} else {
						$('#localitate-group input').css({"border-color": "#c7d0d2", "box-shadow" : "0 0 0 5px #f5f7f8"});
					}
				});
			});
			
			$("#strada").blur(function(){
				var strada = $("#strada").val();
				$.post("<?php echo base_url()."user/address_validation/add/dinamic/"?>",
				{
				  strada: strada
				},
				function(data){
					var obj = $.parseJSON(data);
					$("#strada-group label.error").html(obj.strada_errore);
					if (obj.strada_errore != '') {
						$('#strada-group input').css({"border-color": "#b94a48", "box-shadow" : "0 0 0 5px #f2dede"});
					} else {
						$('#strada-group input').css({"border-color": "#c7d0d2", "box-shadow" : "0 0 0 5px #f5f7f8"});
					}
				});
			});
			
			$("#bloc").blur(function(){
				var bloc = $("#bloc").val();
				$.post("<?php echo base_url()."user/address_validation/add/dinamic/"?>",
				{
				  bloc: bloc
				},
				function(data){
					var obj = $.parseJSON(data);
					$("#bloc-group label.error").html(obj.bloc_errore);
					if (obj.bloc_errore != '') {
						$('#bloc-group input').css({"border-color": "#b94a48", "box-shadow" : "0 0 0 5px #f2dede"});
					} else {
						$('#bloc-group input').css({"border-color": "#c7d0d2", "box-shadow" : "0 0 0 5px #f5f7f8"});
					}
				});
			});
			
			$("#phone").blur(function(){
				var phone = $("#phone").val();
				$.post("<?php echo base_url()."user/address_validation/add/dinamic/"?>",
				{
				  phone: phone
				},
				function(data){
					var obj = $.parseJSON(data);
					$("#phone-group label.error").html(obj.phone_errore);
					if (obj.phone_errore != '') {
						$('#phone-group input').css({"border-color": "#b94a48", "box-shadow" : "0 0 0 5px #f2dede"});
					} else {
						$('#phone-group input').css({"border-color": "#c7d0d2", "box-shadow" : "0 0 0 5px #f5f7f8"});
					}
				});
			});
			
		});
	</script>