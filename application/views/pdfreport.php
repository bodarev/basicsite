<?php
//============================================================+
// File name   : example_005.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 005 for TCPDF class
//               Multicell
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Multicell
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
//require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Vasile Bodarev');
$pdf->SetTitle('Report');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array('lato', '', 12));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('lato', '', 14);

// add a page
$pdf->AddPage();

// set cell padding
$pdf->setCellPaddings(1, 1, 1, 1);

// set cell margins
$pdf->setCellMargins(1, 1, 1, 1);

// set color for background
$pdf->SetFillColor(255, 255, 127);


//$style = file_get_contents(base_url().'bootstrap/css/bootstrap.min.css');

$title = <<<EOF
	<h1 style="font-family:robotoslab; color: #da4453; font-size:36px;">$orders_title</h1>
EOF;

$pdf->WriteHTMLCell(0, 0, '', '', $title, 0, 1, 0, true, 'C', true);

$html = '<style>

table td {

	border: solid 1px #999;
	height:30px;
	line-height:30px;
}

table th {
	height:40px;
	line-height:30px;
	background-color: #eee;
	border: solid 1px #999;
}

</style>';

$html .= '<table> 
			<tr>
				<th colspan="2">'.$orderDate.'</th>
				<th colspan="1">'.$orderId.'</th>
				<th colspan="1">'.$orderStatus.'</th>
				<th colspan="1">'.$orderTotal.'</th>
			</tr>';

   
   foreach ($result as $row)
   {   
	   // pregatim variabilele pentru display
   
	   $total = $row->total;
	   if ( $row->reception == 1 ) {
		   $total = $total + $row->delivery;
	   }
	   
	   $day = date("j ", $row->date);
	   $month = date("F", $row->date);
	   $year = date(" Y H:i", $row->date);
	   $date = $day.${$month}.$year;
	   
	   $status = ${$row->status};
	   
	   $html .= '<tr>    
			   <td colspan="2">'.$date.'</td>
			   <td colspan="1">'.$row->id_order.'</td>
			   <td colspan="1">'.$status.'</td>
			   <td colspan="1">'.$total.'&nbsp;'.$currency_mdl.'</td>
			   </tr>';
   }

   $html .= '</table>';

$pdf->WriteHTMLCell(0, 0, '', '', $html , 0, 1, 0, true, 'C', true);

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('report.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
