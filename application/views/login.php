<?php include 'lang.php'; ?>    
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>

<div class="container">
  <div class="col-lg-8 col-md-8">
    <div class="content"> <?php echo heading ($login_title,1);
			 echo heading ($login_desc,4);	
			 
			$email_errore = form_error('email');
			$pass_errore = form_error('password');
			?>
			
            <a href='<?php echo base_url()."user/signup"?>'> <? echo $LinkRegister; ?> </a>|
             <a href='<?php echo base_url()."user/remember"?>'><? echo $LinkRemember; ?> </a>
            
           	<form action="<?php echo base_url()."user/login_validation"?>" method="post" accept-charset="utf-8" id="login">
                <div class="row">
                	<div class="col-md-5">
                        <span class="my-input-group">
                            <i class="fa fa-envelope-o fa-lg"></i>
                            <input type="text" name="email" placeholder="Email address" value="<?php echo $this->input->post('email')?>">
                        </span>
                    </div>
                    <div class="col-md-7">    
                        <label class="error"><? echo $email_errore; ?></label>
                    </div>    
                </div>
                
                <div class="row">
                	<div class="col-md-5">
                        <span class="my-input-group">
                        	<i class="fa fa-lock fa-lg"></i>
                        	<input type="password" name="password" placeholder="Password" value="" >
                        </span>
                    </div>
                    <div class="col-md-7">    
                        <label class="error"><? echo $pass_errore; ?></label>
                    </div>    
                </div>
                <br />
                <p><input class="btn btn-success btn-embossed" type="submit" name="login_submit" value="<? echo $ButtonLogin;?>"></p>
			</form> 
                                    
			 </div>
  </div>
  <?php include 'right_box.php'; ?>
</div>

<?php include 'footer.php'; ?>