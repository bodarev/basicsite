<?php include 'lang.php'; ?>
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>
<div class="container">
	<div class="col-md-12">
		<div class="content">
			<div class="dropdown row">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<button class="dropbtn btn btn-success btn-lg btn-embossed" type="button" lass="dropdown-toggle" data-toggle="dropdown">
						<?php echo $choose_category; ?> 
						<i class="fa fa-chevron-down fa-fw"> </i> 
					</button>
					<ul class="dropdown-menu list-category">
						<li class="category" id="0"> <i class="fa fa-bars fa-fw"> </i> &nbsp;
							<?php echo $all_category; ?>
						</li>
						<?php $group = '';
						foreach($category as $rowcat) {    
							if ( $group == $rowcat->name_group ) { ?>
							<li class="category" id="<?php echo $rowcat->id; ?>"> <i class="fa <?php echo $rowcat->icon_category; ?> fa-fw"> </i> &nbsp;
								<?php echo "${$rowcat->name_category}"; ?>
							</li>
							<?php } else { 
								if ($rowcat->name_group != $rowcat->name_category) { ?>
								<?php $group = $rowcat->name_group; ?>
									<li class="group">
										<?php echo ${$group};?>
									</li>
									<?php } ?>
										<li class="category" id="<?php echo $rowcat->id; ?>"> <i class="fa <?php echo $rowcat->icon_category; ?> fa-fw"> </i> &nbsp;
											<?php echo ${$rowcat->name_category}; ?>
										</li>
										<?php }

						}?>
					</ul>
				</div>
				<div class="name-category col-md-8 col-sm-6 col-xs-12">
					<h3><?php echo $all_category; ?></h3>
				</div>
			</div>
			<!--/.dropdown-->
			<div class="products row">
				<?
					foreach ($result as $row)
					{
						echo '<article class="col col-md-4 col-sm-6 col-xs-12" align="center"> 
								<div class="modalClick" data-toggle="modal" data-target="#myModal'.$row->id_prod.'">
									<img  class="img-responsive img-rounded" src="'.base_url().$row->thumb_img_prod.'" alt="'.$row->id_prod.'" />													
									<h3 class="title_prod">'.$row->title_prod.'</h3>
								</div>
								<div class="secondamt">  
									<div class="price">'.$row->pret_prod.'&nbsp;'.$currency_mdl.'</div> 
									<a class="buy btn" id="'.$row->id_prod.'" ><i class="fa fa-shopping-cart fa-fw"></i> &nbsp; '.$bagAdded.'</a>
									<div class="amt">
										<input type="button" class="less btn">
										<input type="text" name="" id="quantid'.$row->id_prod.'" value="1">
										<input type="button" class="more btn">
									</div>
								</div>
							  </article>
							
							<div class="modal fade" id="myModal'.$row->id_prod.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">	
								<div class="modal-dialog" role="document">
									<div class="modal-content">
									  	<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">'.$row->title_prod.'</h4>
									  	</div>
									 	<div class="modal-body">
									  		<img class="img-responsive img-rounded" src="'.base_url().$row->orig_img_prod.'" alt="'.$row->id_prod.'" />
											<div align="left" class="desc_prod">'.$row->desc_prod.'</div>
										</div>
									</div>
								</div>
							</div>';
					}
				?>
			</div>
		</div>
	</div>
</div>
<?php include 'footer.php'; ?>
<script>
	$(document).ready(function () {
		/* Plus-minus cantitatea */
		$(document).on('click', 'input.more', function () {
			valTemp = $(this).siblings('input[type=text]').val();
			valTemp++;
			$(this).siblings('input[type=text]').val(valTemp);
		});
		$(document).on('click', 'input.less', function () {
			valTemp = $(this).siblings('input[type=text]').val();
			valTemp--;
			if (valTemp == 0) {}
			else {
				$(this).siblings('input[type=text]').val(valTemp);
			}
		});
		/* La hover ascunde pretul si afiseaza cantiatea */
		$(document).on('mouseenter', 'article', function () {
			var width = $(window).width();
			if (width > 768) {
				$(this).find('.amt').css("display", "block");
				$(this).find('.price').css("display", "none");
			}
		});
		$(document).on('mouseleave', 'article', function () {
			var width = $(window).width();
			if (width > 768) {
				$(this).find('.amt').css("display", "none");
				$(this).find('.price').css("display", "block");
			}
		});
		/* Trimite la host id + cantitatea si reintoarce noul numarul total de produse */
		$(document).on('click', 'a.buy', function () {
			var id = this.id;
			var contor = $(this).closest("article").find('input[type=text]').val();
			var price = $(this).closest("article").find('.price').text();
			price = parseInt(price);
			$.ajax({
				url: "<?php echo base_url().'user/add_product_to_cart/'?>"
				, type: 'POST'
				, data: {
					id: id
					, count: contor
					, price: price
				}
			}).done(function (result) {
				$(".header-cart").empty();
				$(".header-cart").append('<a href="<?php echo base_url(); ?>user/bag"><span class="cart-title"><i class="fa fa-shopping-cart fa-fw"></i>&nbsp;<?php echo $bag_title; ?></span><span id="total-cart-count" class="my-badge">' + result + '</span></a>');
			});
		});
		/* Afiseaza rezultatele dupa ce alegi categoria */
		$(document).on('click', 'ul li', function () {
			var catID = $(this).attr('id');
			var category = $(this).text();
			category = category.trim();
			$(".products").empty();
			$(".name-category").empty();
			$(".name-category").append('<h3>' + category + '</h3>');
			$.ajax({
				url: "<?php echo base_url().'user/load_personal_menu/'?>"
				, type: 'POST'
				, data: {
					valore: catID
				}
			}).done(function (result) {
				var obj = $.parseJSON(result);
				$.each(obj, function (index, value) {
					$(".products").append('<article class="col col-md-4 col-sm-6 col-xs-12" align="center"><div class="modalClick" data-toggle="modal" data-target="#myModal' + value.id_prod + '"><img class="img-responsive img-rounded" src="' + "<?php echo base_url(); ?>" + value.thumb_img_prod + '" alt="' + value.id_prod + '"/><h3 class="title_prod">' + value.title_prod + '</h3></div><div class="secondamt"><div class="price">' + value.pret_prod + '&nbsp;' + "<?php echo $currency_mdl; ?>" + '</div><a class="buy btn" id="' + value.id_prod + '"><i class="fa fa-shopping-cart fa-fw"></i>&nbsp;' + "<?php echo $bagAdded; ?>" + '</a><div class="amt"><input type="button" class="less btn"><input type="text" name="" id="quantid' + value.id_prod + '" value="1"><input type="button" class="more btn"></div></div></article><div class="modal fade" id="myModal' + value.id_prod + '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="myModalLabel">' + value.title_prod + '</h4></div><div class="modal-body"><img class="img-responsive img-rounded" src="' + "<?php echo base_url(); ?>" + value.orig_img_prod + '" alt="' + value.id_prod + '"/><div align="left" class="desc_prod">' + value.desc_prod + '</div></div></div></div></div>');
				});
				if (obj == '') {
					$(".products").append('' + "<?php echo heading ($search_not_found,3); ?>" + '');
				}
			});
		});
	});
</script>