  <?php include 'lang.php'; ?>
  <?php 
				$title = "Info!";
				$current = "login";
		?>
        
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>

<div class="container">
  <div class="col-lg-8 col-md-8">
    <div class="content">
    <br />
			  <?php if($tip == 'error') { ?>
				  	<p class="alert-msg alert-error"><?php echo $message;?></p>
				 <? } else { ?>
					  
					  <p class="alert-msg alert-success"><?php echo $message;?></p>
					<?  } ?>	 
			  
			 </div>
  </div>
  <?php include 'right_box.php'; ?>
</div>

<?php include 'footer.php'; ?>