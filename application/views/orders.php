<?php include 'lang.php'; ?>
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>
<div class="container">
    <div class="col-lg-8 col-md-8">
        <div class="content">
            <?php echo heading ($orders_title,1);	 ?>
            <section>
                 <article class="order">
                    <header>
                        <div class="col-sm-4">
                            <?php echo $orderDate; ?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $orderId; ?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $orderStatus; ?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $orderTotal; ?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $orderDetails; ?>
                        </div>
                    </header>
                    <?php foreach ($result as $row)
                        {   
							// pregatim variabilele pentru display
						
						    $total = $row->total;
                            if ( $row->reception == 1 ) {
                                $total = $total + $row->delivery;
                            }
							
							$day = date("j ", $row->date);
							$month = date("F", $row->date);
							$year = date(" Y H:i", $row->date);
							$date = $day.${$month}.$year;
							
							$status = ${$row->status};
							
                            echo '<figure>    
                                <div class="order-header clearfix">    
                                    <div class="col-sm-4">
										<span class="vertical-column-name">'.$orderDate.': </span>'.$date.'
                                    </div>
									<div class="col-sm-2">
                                        <span class="vertical-column-name">'.$orderId.': </span>'.$row->id_order.'
                                    </div>
                                    <div class="col-sm-2 label '; 
									
									// adaugam culorile in dependenta de status
									
									switch($row->status) {
										case "waiting":
											echo 'label-warning';
											break;
										case "processed":
											echo 'label-success';
											break;
										default:
											echo 'label-danger';
									}
										
									echo '">
                                        <span class="vertical-column-name">'.$orderStatus.': </span>'.$status.'
                                    </div>
                                    <div class="col-sm-2">
                                        <span class="vertical-column-name">'.$orderTotal.': </span>'.$total.'&nbsp;'.$currency_mdl.'
                                    </div>
                                    <div class="col-sm-2">
                                        <a id="'.$row->id_order.'" class="show-body">'.$LinkView.'</a>
                                    </div>
                                 </div>   
                                <div class="col-sm-12 order-body clearfix" id="'.$row->id_order.'">
                                    
                                    <div class="order-info">
                                    </div>';
                                    
                                    if ($row->delivery > 0) {
                                        echo '<div class="order-delivery"> 
                                                <div class="col-xs-8"> 
                                                    '.$bagDelivery.'
                                                </div>
                                                <div class="col-xs-4"> 
                                                    '.$row->delivery.' '.$currency_mdl.'
                                                </div>
                                            </div>';
                                    }                                                    
    
                                    if ($row->status == 'waiting' ) {
                                        echo '<div class="order-canceled">
                                                <div class="col-sm-12">
                                                    <a href="'.base_url().'user/cancel_order/'.$row->id_order.'">
                                                        <i class="fa fa-trash-o fa-fw"></i>&nbsp;'.$LinkCancel.'
                                                    </a>
                                                </div>
                                            </div>';
                                    } 
                                echo '</div></figure>';
                        }
                
                        if ($result == null) {
                            echo '
                        <figure>
                            <div class="col-xs-12 order-header">'.$orderEmpty.'</div>
                        </figure>
                    </article>   
                </section>';
                    }
                    ?>
                    <br>
                    <section>
                    <div class="left" style="width:100%">
                    <a href="<?php echo base_url('report') ?>">
                        <input class="btn btn-success btn-embossed" data-toggle="modal" data-target="#myModal" type="submit" name="signup_submit" value="<?php echo $ButtonDownloadPdf; ?>"> 
                        </a>
                    </div>
                    </section>
        </div>
    </div>
    <?php include 'right_box.php'; ?>
</div>
<?php include 'footer.php'; ?>

<script>
    $(document).ready(function() {
        $(document).on('click', 'a.show-body', function() {
            valID = $(this).attr('id');
            curentDIV = 'div#' + valID;
            $(curentDIV + " div.order-info").empty();
            $(this).empty();
            $(this).append('<?php echo $LinkHide;?>');
            $(this).removeClass('show-body');
            $(this).addClass('hide-body');
            
            $.ajax({
                    url: "<?php echo base_url().'user/load_order_info/'?>",
                    type: 'POST',
                    data: {
                        valore: valID
                    }
                })
                .done(function(result) {
                    var obj = $.parseJSON(result);
                    $.each(obj, function(index, value) {
                        $(curentDIV + " div.order-info").append('<div class="col-xs-6">' + value.title_prod + '</div><div class="col-xs-1">' + value.pret_prod + '&nbsp;' + "<?php echo $currency_mdl; ?>" + '</div><div class="col-xs-1 x-count">x' + value.quantity + '</div><div class="col-xs-4">' + value.quantity * value.pret_prod + '&nbsp;' + "<?php echo $currency_mdl; ?>" + '</div>');
                    });
                    $(curentDIV).show("slow");
                });
        });
    });

    $(document).ready(function() {
        $(document).on('click', 'a.hide-body', function() {
            valID = $(this).attr('id');
            curentDIV = 'div#' + valID;
            $(curentDIV).hide("slow");
            $(this).empty();
            $(this).append('<?php echo $LinkView;?>');
            $(this).removeClass('hide-body');
            $(this).addClass('show-body');
        });
    });

</script>
