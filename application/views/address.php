<?php include 'lang.php'; ?>
	<?php include 'header.php'; ?>
		<?php include 'navigation.php'; ?>
			<div class="container">
				<div class="col-lg-8 col-md-8">
					<div class="content">
						 <?php echo heading ($address_title,1);	?>
							<div class="address">
								<? $index = 0;
					foreach ($result as $row)
					{
						$index ++;
						echo '
 
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading'.$index.'">
                              <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'.$index.'" aria-expanded="true" aria-controls="collapse'.$index.'">'
                                  .$user_address.'&nbsp'.$index.'
                                </a>
                              </h4>
                            </div>
                            <div id="collapse'.$index.'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading'.$index.'">
                              <div class="panel-body">
                        
                        		<div class="col-sm-12">
									'.$row->name.'<br />'
									.$row->strada.',&nbsp'
									.$row->bloc;


									if ($row->scara > 0) {	
										echo ',&nbsp'.$user_entrance.'&nbsp'
											.$row->scara;
									}
									if ($row->etaj > 0) {
										echo ',&nbsp'.$user_floor.'&nbsp'
											.$row->etaj;
									}
									if ($row->apartament > 0) {
										echo ',&nbsp'.$user_apartament_short.'&nbsp'
											.$row->apartament;
									}
									echo '<br />'		
									.$row->localitate.'<br />'
									.$user_phone.':&nbsp'
									.$row->phone.'<br />E-mail:&nbsp'
									.$row->email.'
								
								</div>
								
								<div class="col-sm-12">
									<a href="'.base_url().'user/edit_address_user/'.$row->id.'">
                                	<i class="fa fa-pencil fa-fw"></i>&nbsp;'.$LinkEditAddress.'</a>
								</div>
								<div class="col-sm-8">
									<a href="'.base_url().'user/set_default_address_user/'.$row->id.'">
									<i class="fa fa-flag fa-fw"></i>&nbsp;'.$LinkSetDefaultAddress.'</a>
								</div>
								<div class="col-sm-4">
									<a class="right" href="'.base_url().'user/delete_address_user/'.$row->id.'">
									<i class="fa fa-trash-o fa-fw"></i>&nbsp;'.$LinkDelete.'</a> 
								</div>
                                 </div>
                            </div>
                          </div>
                          
                          
                          ';
					}
			
					echo '<a href="'.base_url().'user/add_address_user/"><i class="fa fa-plus fa-fw"></i>&nbsp;'.$LinkAddAddress.'</a><br />';
			
			?> </div>
					</div>
				</div>
				<?php include 'right_box.php'; ?>
			</div>
			<?php include 'footer.php'; ?>