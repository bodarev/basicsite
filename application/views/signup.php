<?php include 'lang.php'; ?>       
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>

<div class="container">
	<div class="col-lg-8 col-md-8">
		<div class="content"> 
			<?php echo heading ($signup_title,1);
				echo heading ($signup_desc,4);	
				 
				$email_errore = form_error('email');
				$pass_errore = form_error('password');
				$cpass_errore = form_error('cpassword');	 
			?>
            
             <a href='<?php echo base_url()."user/login"?>'> <? echo $LinkLogin; ?> </a>|
             <a href='<?php echo base_url()."user/remember"?>'><? echo $LinkRemember; ?> </a>
            
			<form action="<?php echo base_url()."user/signup_validation"?>" method="post" accept-charset="utf-8" id="register">        
				
                <div id="email-group" class="row">
                	<div class="col-md-5">
                        <span class="my-input-group">
                            <i class="fa fa-envelope-o fa-lg"></i>
                            <input type="text" name="email" placeholder="Email address" value="<?php echo $this->input->post('email')?>" id="email">
                        </span>
                    </div>
                    <div class="col-md-7">    
                        <label class="error"><? echo $email_errore; ?></label>
                    </div>    
			 	</div>	                    
         
                <div id="password-group" class="row">
                	<div class="col-md-5">
                        <span class="my-input-group">
                            <i class="fa fa-lock fa-lg"></i>
                            <input type="password" name="password" placeholder="Password" value="" id="password">
                        </span>
                    </div>
                    <div class="col-md-7">    
                        <label class="error"><? echo $pass_errore; ?></label>	
                    </div>    
				</div>
					
				<div id="cpassword-group" class="row">
                	<div class="col-md-5">
                        <span class="my-input-group">
                            <i class="fa fa-lock fa-lg"></i> 
                            <input type="password" name="cpassword" placeholder="Confirm Password"value="" disabled id="cpassword"> 
                        </span>
                    </div>
                    <div class="col-md-7">    
                        <label class="error"><? echo $cpass_errore; ?></label>
                    </div>    
				</div>
				<br />
				<p><input class="btn btn-success btn-embossed" type="submit" name="signup_submit" value="<? echo $ButtonRegister; ?>"></p>
			</form> 
            
            <br/>    		  
		</div>
	</div>
  <?php include 'right_box.php'; ?>
  <script>	
		$(document).ready(function(){
			$("#email").blur(function(){
				var mail = $("#email").val();
				$.post("<?php echo base_url()."user/form_validation_signup/param"?>",
				{
				  email: mail
				},
				function(data){
					var obj = $.parseJSON(data);
					$("#email-group label.error").html(obj.email_errore);
				});
			});
			$("#password").blur(function(){
				var pass = $("#password").val();
				var cpass = $("#cpassword").val();
				$.post("<?php echo base_url()."user/form_validation_signup/param"?>",
				{
				  password: pass,
				  cpassword: cpass
				},
				function(data){
					obj = $.parseJSON(data);
					$("#password-group label.error").html(obj.pass_errore);
					if ($("#cpassword").val().length > 0 ) {
						$("#cpassword-group label.error").html(obj.cpass_errore);
					}
				});
			});
			$("#cpassword").blur(function(){
				var pass = $("#password").val();
				var cpass = $("#cpassword").val();
				$.post("<?php echo base_url()."user/form_validation_signup/param"?>",
				{
				  password: pass,
				  cpassword: cpass
				},
				function(data){
					var obj = $.parseJSON(data);
					$("#cpassword-group label.error").html(obj.cpass_errore);
				});
			});
		});
	</script>

</div>

<?php include 'footer.php'; ?>