<?php include 'lang.php'; ?>       
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>

<div class="container">
	<div class="col-lg-8 col-md-8">
		<div class="content"> 
			<?php echo heading ($remember_title,1);	
				 
				$email_errore = form_error('email');	 
			?>
            
             <a href='<?php echo base_url()."user/signup"?>'> <? echo $LinkRegister; ?> </a>|
             <a href='<?php echo base_url()."user/login"?>'><? echo $LinkLogin; ?> </a>
            
			<form action="<?php echo base_url()."user/send_password"?>" method="post" accept-charset="utf-8" id="register">        
				<div id="email-group" class="row">
                	<div class="col-md-5">        
                        <span class="my-input-group">
                            <i class="fa fa-envelope-o fa-lg"></i>
                            <input type="text" name="email" placeholder="Email address" value="<?php echo $this->input->post('email')?>" id="email">
                        </span> 
                    </div>
                    <div class="col-md-7">    
                        <label class="error"><? echo $email_errore; ?></label>
                    </div>    
				 </div>			
				<br />
				<p><input class="btn btn-success btn-embossed" type="submit" name="signup_submit" value="<? echo $ButtonRemember; ?>"></p>
			</form> 		  
		</div>
	</div>
  <?php include 'right_box.php'; ?>
  <script>	
		$(document).ready(function(){
			$("#email").blur(function(){
				var mail = $("#email").val();
				$.post("<?php echo base_url()."user/form_validation_remember/param"?>",
				{
				  email: mail
				},
				function(data){
					var obj = $.parseJSON(data);
					$("#email-group label.error").html(obj.email_errore);
				});
			});
		});
	</script>

</div>

<?php include 'footer.php'; ?>