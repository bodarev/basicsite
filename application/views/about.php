<?php include 'lang.php'; ?>       
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>

<div class="container">
	<div class="col-lg-8 col-md-8">
		<div class="content"> 
			<?php 
				echo heading ($about_title,1);
				echo heading ($about_desc,4);
			?>      
		</div>
	</div>
	<?php include 'right_box.php'; ?>
</div>

<?php include 'footer.php'; ?>