<div id="float">
	<a href="#" id="touch-menu">
		<div class="mobile-navigation"> <span></span> </div>
	</a>
	<ul class="navigation">
		<li <?php if($current=="home" ) echo "class='current'";?> >
			<a href="<?php echo base_url(); ?>site/home"> <i class="fa fa-home fa-fw"></i>&nbsp;
				<? echo $home; ?>
			</a>
		</li>
		<li <?php if($current=="about" ) echo "class='current'";?> >
			<a href="<?php echo base_url(); ?>site/about"> <i class="fa fa-info fa-fw"></i>&nbsp;
				<? echo $about; ?>
			</a>
		</li>
		<li <?php if($current=="menu" ) echo "class='current'";?> >
			<a href="<?php echo base_url(); ?>user/menu"> <i class="fa fa-cutlery fa-fw"></i>&nbsp;
				<? echo $menu; ?>
			</a>
		</li>
		<li <?php if($current=="news" ) echo "class='current'";?>>
			<a href="<?php echo base_url(); ?>site/news"> <i class="fa fa-file-text-o fa-fw"></i>&nbsp;
				<? echo $news; ?>
			</a>
		</li>
		
		<li class="<?php if($current=="personal" or $current=="orders" or $current=="address" or $current=="login" or $current=="remember" or $current=="signup" ) echo "current ";?>dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp;
				<? echo $login; ?>
				<i class="fa fa-caret-down" aria-hidden="true"></i>
			</a>
			<?php if ($this->session->userdata('is_logged_in')) { ?>
					
					<ul class="dropdown-menu">
						<li>
							<div class="tri-up"></div>
						</li>
						<li class=" <?php if($current==" personal ") echo "active ";?>">
							<a href='<?php echo base_url()."user/personal"?>'>
								<i class="fa fa-user fa-fw"></i>
								<span><? echo $myAcaunt;?></span>
							</a>
						</li>
						<li class=" <?php if($current==" orders ") echo "active ";?>">
							<a href='<?php echo base_url()."user/orders"?>'>
								<i class="fa fa-history fa-fw"></i> 
								<span><? echo $historyOrders;?></span>
							</a>
						</li>
						<li class=" <?php if($current==" address ") echo "active ";?>">
							<a href='<?php echo base_url()."user/address"?>'>
								<i class="fa fa-map-marker fa-fw"></i>
								<span><? echo $address;?></span>
							</a>
						</li>
						<li class="divider"></li>
						<li class="">
							<a href='<?php echo base_url()."user/logout"?>'>
								<i class="fa fa-sign-out fa-fw"></i>
								<span><? echo $LinkLogout;?></span>
							</a>
						</li>
					</ul>
			<?php } else { ?>
				<ul class="dropdown-menu">
					<li>
						<div class="tri-up"></div>
					</li>
					<li class="">
						<a href='<?php echo base_url()."user/login"?>'>
							<i class="fa fa-sign-in fa-fw"></i>
							<span><? echo $LinkLogin;?></span>
						</a>
					</li>
					<li class="">
						<a href='<?php echo base_url()."user/signup"?>'>
							<i class="fa fa-plug fa-fw"></i>
							<span><? echo $LinkRegister;?></span>
						</a>
					</li>
				</ul>
			<?php } ?>
		</li>
		
		<li <?php if($current=="contact" ) echo "class='current'";?> >
			<a href="<?php echo base_url(); ?>site/contact"> <i class="fa fa-book" aria-hidden="true"></i>&nbsp;
				<? echo $contact; ?>
			</a>
		</li>
	</ul>
</div> <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>