<?php echo doctype("html5"); ?>
	<!DOCTYPE html>
	<html lang="en">

	<head>
		<!--  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
		<meta charset="<?php echo $this->config->item('charset');?>">
		<meta http-equiv="X-UA-Compatible" content="IE=9" />
		<title><?php echo $tab_title; ?> :: Basic Site</title>
		<link rel="icon" href="<?php echo base_url(); ?>img/icon.png" type="image/x-icon" />
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<LINK href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<LINK href="<?php echo base_url(); ?>css/fonts.css" rel="stylesheet" type="text/css">
		<LINK href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css">
		<LINK href="<?php echo base_url(); ?>css/navigation.css" rel="stylesheet" type="text/css">
		<LINK href="<?php echo base_url(); ?>css/modifiche.css" rel="stylesheet" type="text/css">
		<LINK href="<?php echo base_url(); ?>css/buy.css" rel="stylesheet" type="text/css">
		<LINK href="<?php echo base_url(); ?>css/list-category.css" rel="stylesheet" type="text/css">  
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
        <script src="<?php echo base_url(); ?>scripts/MaskedInput.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>scripts/myJquery.js" type="text/javascript"></script>   
        
		<script>
		/* scoate numarul total de produse la incarcarea paginii */
			function getCartCountProd() {
				$.ajax({
					url: "<?php echo base_url().'user/get_quantity_from_cart/'?>"
					, type: 'POST'
					, data: {}
				}).done(function (result) {
					$(".header-cart").empty();
					$(".header-cart").append('<a href="<?php echo base_url(); ?>user/bag"><span class="cart-title"><i class="fa fa-shopping-cart fa-fw"></i>&nbsp;<?php echo $bag_title; ?></span><span id="total-cart-count" class="my-badge">' + result + '</span></a>');
				});
			}
			getCartCountProd();
		</script>
		
	</head>

	<body>
		<div class="primo">
			<div class="header">
				<?
$link = uri_string();
if (empty($link)) {
$link = 'null';	
} else {
$link =  str_replace("/","-", $link);	
}
?>
					<div class="header_mini_menu"> <span class="header-cart">
<a href="<?php echo base_url(); ?>user/bag">	
<span class="cart-title"><i class="fa fa-shopping-cart fa-fw"></i>&nbsp;<?php echo $bag_title; ?></span> <span id="total-cart-count" class="my-badge">0</span> </a>
						</span> <span class="langue">
<a href='<?php echo base_url()."site/langue/Ro/".$link ?>'><img src="<?php echo base_url(); ?>/flags-iso/shiny/32/RO.png" width="32" height="32" /></a>  
<a href='<?php echo base_url()."site/langue/It/".$link ?>'><img src="<?php echo base_url(); ?>/flags-iso/shiny/32/IT.png" width="32" height="32" /></a>
<a href='<?php echo base_url()."site/langue/Ru/".$link ?>'><img src="<?php echo base_url(); ?>/flags-iso/shiny/32/RU.png" width="32" height="32" /></a>
</span> </div>
			</div>
		</div>