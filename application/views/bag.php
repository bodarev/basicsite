<?php include 'lang.php'; ?>
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>
<div class="container">
    <div class="col-md-12">
        <div class="content">
            <?php echo heading ($bag_title,1); ?>
            <?
				$client_errore = form_error('AddressClient');
				$restaurant_errore = form_error('AddressRestaurant');
            
				if (empty($result)) {
				    echo heading ($bagEmpty,4);
				}else { ?>
                <div class="cart-item">
                    <table class="table-cart-products">
                        <thead>
                            <tr>
                                <td class="col-md-2">
                                    <?php echo $bagPhoto; ?>
                                </td>
                                <td class="col-md-6">
                                    <?php echo $bagProduct; ?>
                                </td>
                                <td class="col-md-2">
                                    <?php echo $bagQuantity; ?>
                                </td>
                                <td class="col-md-1">
                                    <?php echo $bagPrice; ?>
                                </td>
                                <td class="col-md-1"></td>
                            </tr>
                        </thead>
                        <tbody class="rows">
                            <?php foreach ($result as $row)
						{
							echo '<tr>
									<td>
										<img src="'.base_url().$row->thumb_img_prod.'" alt="'.$row->id_prod.'" width="128"/>
									</td>
									<td>
										<h2>'.$row->title_prod.'</h2>
										<div>'.$row->desc_prod.'</div>
									</td>
									<td>
										<div class="amt">
											<input type="button" class="less btn">
											<input type="text" name="" id="quantid'.$row->id_prod.'" value="'.$row->quantity.'">
											<input type="button" class="more btn">
										</div>
									</td>
									<td>
										<div class="price">'.$row->pret_prod.'&nbsp;'.$currency_mdl.'</div>
									</td>
									<td>
										<button type="button" class="close delete-product"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
									</td>
								</tr>';	
						} ?>
                        </tbody>
                        <tbody class="total">
                            <tr class="row-price">
                                <td class="total-desc" colspan="3">
                                    <?php echo $bagTotal; ?>
                                </td>
                                <td colspan="2">
                                    <div class="total-price">
                                        <?php echo '---'; ?>&nbsp;
                                        <?php echo $currency_mdl; ?>
                                    </div>
                                </td>
                            </tr>

                            <tr class="row-price">
                                <td class="total-desc" colspan="3">
                                    <?php echo $bagDelivery; ?>
                                </td>
                                <td colspan="2">
                                    <div class="delivery-price">
                                        <?php echo '---'; ?>&nbsp;
                                        <?php echo $currency_mdl; ?>
                                    </div>
                                </td>
                            </tr>

                            <tr class="row-price">
                                <td class="total-desc" colspan="3">
                                    <?php echo $bagTotalDelivery; ?>
                                </td>
                                <td colspan="2">
                                    <div class="total-delivery-price">
                                        <?php echo '---'; ?>&nbsp;
                                        <?php echo $currency_mdl; ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br />
                <div class="left" style="width:100%">
                    <input class="btn btn-success btn-embossed" data-toggle="modal" data-target="#myModal" type="submit" name="signup_submit" value="<?php echo $ButtonOrder; ?>">
                </div>


                <?php }
				?>

        </div>
    </div>
</div>

<?php include 'footer.php'; ?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-order" role="document">
        <form action="<?php echo base_url()."user/add_order "?>" method="post" accept-charset="utf-8" id="order">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">
                        <?php echo $yourOrder; ?>
                    </h4>
                </div>
                <div class="modal-body">
                    <table class="order">
                        <tbody class="total">
                            <tr class="row-price">
                                <td class="total-desc">
                                    <?php echo $bagTotal; ?>
                                </td>
                                <td>
                                    <div class="total-price">
                                        <?php echo '---'; ?>&nbsp;
                                        <?php echo $currency_mdl; ?>
                                    </div>
                                </td>
                            </tr>
                            <tr class="row-price">
                                <td class="total-desc">
                                    <?php echo $bagDelivery; ?>
                                </td>
                                <td>
                                    <div class="delivery-price">
                                        <?php echo '---'; ?>&nbsp;
                                        <?php echo $currency_mdl; ?>
                                    </div>
                                </td>
                            </tr>
                            <tr class="row-price">
                                <td class="total-desc">
                                    <?php echo $bagTotalDelivery; ?>
                                </td>
                                <td>
                                    <div class="total-delivery-price">
                                        <?php echo '---'; ?>&nbsp;
                                        <?php echo $currency_mdl; ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>


                    <h4>
                        <? echo $orderPayMethod;?>
                    </h4>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary btn-embossed" id="orderCash">
                            <input type="radio" name="option1" id="option1" autocomplete="off">
                                <?php echo $orderCash; ?>
                        </label>
                        <label class="btn btn-primary btn-embossed" id="orderBankingTerminal">
                            <input type="radio" name="option2" id="option2" autocomplete="off">
                                <?php echo $orderBankingTerminal; ?>
                        </label>
                    </div>

                    <h4>
                        <? echo $orderReception;?>
                    </h4>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-warning btn-embossed" id="orderDelivery">
                            <input type="radio" name="option3" id="option3" autocomplete="off"> 
                                <?php echo $orderDelivery; ?>				
                        </label>
                        <label class="btn btn-warning btn-embossed" id="orderFromRestaurant">
                            <input type="radio" name="option4" id="option4" autocomplete="off"> 
                                <?php echo $orderFromRestaurant; ?>
                        </label>
                    </div>


                    <div id="orderAddressClient">
                        <h4>
                            <? echo $orderAddress;?>
                        </h4>

                        <?php 
                        if ($this->session->userdata('email') != '') { ?>

                        <select id="AddressClient" name="AddressClient">
                                <?php 
                                    foreach($user_address as $rowaddress) {			
                                        echo '<option value="'.$rowaddress->id.'">'.$rowaddress->localitate.', '.$rowaddress->strada.' '.$rowaddress->bloc.', '.$rowaddress->phone.'</option>';					
                                    }
                                ?>
                            </select>

                        <?php } else { ?>
                        <a href='<?php echo base_url()."user/login/".$link ?>'>
                            <?php echo $LinkLogin; ?> </a>
                        <?php echo $orderForSelectAddress; ?>
                        <?php }
                    ?>
                    </div>

                    <div id="orderAddressRestaurant">
                        <h4>
                            <? echo $orderAddress;?>
                        </h4>

                        <?php 
                        if ($this->session->userdata('email') != '') { ?>

                        <select id="AddressRestaurant" name="AddressRestaurant">
                                <?php 
                                    foreach($restaurant_address as $rowrestaurant) {			
                                        echo '<option value="'.$rowrestaurant->id.'">'.$rowrestaurant->localitate.', '.$rowrestaurant->strada.' '.$rowrestaurant->bloc.', '.$rowrestaurant->phone.'</option>';					
                                    }
                                ?>
                            </select>

                        <?php } else { ?>
                        <a href='<?php echo base_url()."user/login/".$link ?>'>
                            <?php echo $LinkLogin; ?> </a>
                        <?php echo $orderForSelectAddress; ?>
                        <?php }
                    ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <p>
                        <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal"><? echo $ButtonClose;?></button>
                        <input type="submit" class="btn btn-primary btn-embossed" name="order_submit" value="<? echo $ButtonOrder;?>" <?php if ($this->session->userdata('email') == '') { echo 'disabled'; } ?> >
                    </p>
                </div>
            </div>
        </form>
        <?php if ($client_errore != '') { ?> 
            <script> $('select#AddressClient').css({"border-color": "#b94a48", "box-shadow" : "0 0 0 5px #f2dede"}); </script>
        <?php } ?>
        <?php if ($restaurant_errore != '') { ?> 
            <script> $('select#AddressRestaurant').css({"border-color": "#b94a48", "box-shadow" : "0 0 0 5px #f2dede"}); </script>
        <?php } ?>
    </div>
</div>

<script>
    $(document).ready(function() {

        function calcTotalPrice(x) {
            var qCur = 0;
            var pCur = 0;
            var totalCurent = 0;
            var total = 0;
			if (x == 0) {
				var delivery = '<?php echo $amount['delivery']; ?>';
            	delivery = parseInt(delivery);
            	var freeDelivery = '<?php echo $amount['freeDelivery']; ?>';
            	freeDelivery = parseInt(freeDelivery);
			} else {
				var delivery = 0;
            	var freeDelivery = 0;
			}
            
            $(".table-cart-products tbody.rows tr").each(function() {
                qCur = $(this).find('.amt input[type=text]').val();
                pCur = $(this).find(".price").text();
                pCur = parseInt(pCur);
                totalCurent = (pCur * qCur);
                total = total + totalCurent;
            });
            $(".total-price").empty();
            $(".total-price").append(total + '&nbsp;' + "<?php echo $currency_mdl; ?>" + '');
            if (total < freeDelivery) {
                total = total + delivery;
                $(".delivery-price").empty();
                $(".delivery-price").append(delivery + '&nbsp;' + "<?php echo $currency_mdl; ?>" + '');
            } else {
                $(".delivery-price").empty();
                $(".delivery-price").append('---&nbsp;' + "<?php echo $currency_mdl; ?>" + '');
            }
            $(".total-delivery-price").empty();
            $(".total-delivery-price").append(total + '&nbsp;' + "<?php echo $currency_mdl; ?>" + '');
        }
        calcTotalPrice(0);


        /* scoate totalul de produse la incarcarea paginii */

        $(document).on('click', 'button.delete-product', function() {
            var id = $(this).closest("tr").find('img').attr("alt");
            $.ajax({
                    url: "<?php echo base_url().'/user/remove_product_from_cart/'?>",
                    type: 'POST',
                    async: false,
                    data: {
                        id: id
                    }
                })
                .done(function(result) {
                    var obj = $.parseJSON(result);
                    $(".table-cart-products tbody.rows").empty();
                    $.each(obj, function(index, value) {
                        $(".table-cart-products tbody.rows").append('<tr><td><img src="<?php echo base_url(); ?>' + value.thumb_img_prod + '" alt="' + value.id_prod + '" width="128"/></td><td><h2>' + value.title_prod + '</h2><div>' + value.desc_prod + '</div></td><td><div class="amt"><input type="button" class="less btn"><input type="text" name="" id="quantid' + value.id_prod + '" value="' + value.quantity + '"><input type="button" class="more btn"></div></td><td><div class="price">' + value.pret_prod + '&nbsp;' + "<?php echo $currency_mdl; ?>" + '</div></td><td><button type="button" class="close delete-product"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td></tr>');
                    });
                    if (obj == '') {
                        $(".content").empty();
                        $(".content").append('' + "<?php echo heading ($bag_title,1); echo heading ($bagEmpty,4); ?>" + '');
                    } else {
                        $(".table-cart-products tbody.total").empty();
                        $(".table-cart-products tbody.total").append('<tr class="row-price"><td class="total-desc" colspan="3">' + "<?php echo $bagTotal; ?>" + '</td><td colspan="2"><div class="total-price">' + "<?php echo '456'; ?>" + '&nbsp;' + "<?php echo $currency_mdl; ?>" + '</div></td></tr><tr class="row-price"><td class="total-desc" colspan="3">' + "<?php echo $bagDelivery; ?>" + '</td><td colspan="2"><div class="delivery-price">' + "<?php echo '---'; ?>" + '&nbsp;' + "<?php echo $currency_mdl; ?>" + '</div></td></tr><tr class="row-price"><td class="total-desc" colspan="3">' + "<?php echo $bagTotalDelivery; ?>" + '</td><td colspan="2"><div class="total-delivery-price">' + "<?php echo '456'; ?>" + '&nbsp;' + "<?php echo $currency_mdl; ?>" + '</div></td></tr>');
                    }
                });
            getCartCountProd();
            calcTotalPrice(0);
        });

        /* Plus-minus cantitatea */

        $(document).on('click', 'input.more', function() {
            valTemp = $(this).siblings('input[type=text]').val();
            valTemp++;
            $(this).siblings('input[type=text]').val(valTemp);
            var id = $(this).closest('tr').find('img').attr("alt");
            var contor = $(this).siblings('input[type=text]').val();
            var price = $(this).closest("tr").find('.price').text();
            price = parseInt(price);
            addMoreProd(id, contor, price);
            calcTotalPrice(0);
        });

        $(document).on('click', 'input.less', function() {
            valTemp = $(this).siblings('input[type=text]').val();
            valTemp--;
            if (valTemp == 0) {} else {
                $(this).siblings('input[type=text]').val(valTemp);
            }
            var id = $(this).closest('tr').find('img').attr("alt");
            var contor = $(this).siblings('input[type=text]').val();
            var price = $(this).closest("tr").find('.price').text();
            price = parseInt(price);
            addMoreProd(id, contor, price);
            calcTotalPrice(0);
        });


        function addMoreProd(id, contor, price) {
            $.ajax({
                    url: "<?php echo base_url().'user/add_product_to_cart/'?>",
                    type: 'POST',
                    data: {
                        id: id,
                        count: contor,
                        price: price
                    }
                })
                .done(function(result) {
                    $(".header-cart").empty();
                    $(".header-cart").append('<a href="<?php echo base_url(); ?>user/bag"><span class="cart-title"><i class="fa fa-shopping-cart fa-fw"></i>&nbsp;<?php echo $bag_title; ?></span><span id="total-cart-count" class="my-badge">' + result + '</span></a>');
                });
        }
		
		/* Display Modal daca te loghezi din Modal */
		
		var show = '<?php echo $display; ?>';
        if (show == 'block') {
            $('#myModal').modal('show');
        }
		
		/* DeliveryPrice la apasarea butonului preluare */
		
		$(document).on('click', '#orderCash', function() {
            $('label#orderBankingTerminal input').prop('checked', false);
			$('label#orderCash input').attr('checked', true);
        });	
		$(document).on('click', '#orderBankingTerminal', function() {
			$('label#orderCash input').prop('checked', false);
			$('label#orderBankingTerminal input').attr('checked', true);
        });
		
		$(document).on('click', '#orderDelivery', function() {
            $('label#orderFromRestaurant input').prop('checked', false);
			$('label#orderDelivery input').attr('checked', true);
            calcTotalPrice(0);
        });	
		$(document).on('click', '#orderFromRestaurant', function() {
			$('label#orderDelivery input').prop('checked', false);
			$('label#orderFromRestaurant input').attr('checked', true);
            calcTotalPrice(1);
        });
		
		/* Activeaza butoanele  */
		
		var $radio1 = '<?php echo $radio1; ?>'; 
		var $radio2 = '<?php echo $radio2; ?>';   
		if ($radio1 == 1 ) {
			$('label#orderCash').addClass('active');
			$('label#orderBankingTerminal').removeClass('active');
			
			$('label#orderCash input').attr('checked', true);
			$('label#orderBankingTerminal input').attr('checked', false);
		} else {
			$('label#orderCash').removeClass('active');
			$('label#orderBankingTerminal').addClass('active');  
			
			$('label#orderCash input').attr('checked', false);
			$('label#orderBankingTerminal input').attr('checked', true);
		}
		if ($radio2 == 1 ) {
			$('label#orderDelivery').addClass('active');
			$('label#orderFromRestaurant').removeClass('active');
		   
			$('label#orderFromRestaurant input').attr('checked', false);
            $('label#orderDelivery input').attr('checked', true);
					
			$("#orderAddressClient").show("slow");
			$("#orderAddressRestaurant").hide("slow");
			calcTotalPrice(0);
		} else {
			$('label#orderDelivery').removeClass('active');
			$('label#orderFromRestaurant').addClass('active');
		   
			$('label#orderDelivery input').attr('checked', false);
			$('label#orderFromRestaurant input').attr('checked', true);
			
			$("#orderAddressClient").hide("slow");
			$("#orderAddressRestaurant").show("slow");
			calcTotalPrice(1);
		}
			
    });
</script>