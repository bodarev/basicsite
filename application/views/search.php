 <?php include 'lang.php'; ?>
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>

<div class="container">
    <div class="col-lg-8 col-md-8"> 
    	<div class="content"> 
  			
			<?
				if (isset($query) && !empty($query)) { 
					foreach ($query as $row)
					{
						echo '<article class="col col-xs-12" align="center" style="width:95%;"> 
								<div class="modalClick" data-toggle="modal" data-target="#myModal'.$row->id_prod.'">
									<img  class="img-responsive img-rounded" src="'.base_url().$row->thumb_img_prod.'" alt="'.$row->id_prod.'" />													
									<h3 class="title_prod">'.$row->title_prod.'</h3>
								</div>
								<div class="secondamt">  
									<div class="price">'.$row->pret_prod.'&nbsp;'.$currency_mdl.'</div> 
									<a class="buy btn" id="'.$row->id_prod.'" ><i class="fa fa-shopping-cart fa-fw"></i> &nbsp; '.$bagAdded.'</a>
									<div class="amt">
										<input type="button" class="less btn">
										<input type="text" name="" id="quantid'.$row->id_prod.'" value="1">
										<input type="button" class="more btn">
									</div>
								</div>
							  </article>
							
							<div class="modal fade" id="myModal'.$row->id_prod.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">	
								<div class="modal-dialog" role="document">
									<div class="modal-content">
									  	<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">'.$row->title_prod.'</h4>
									  	</div>
									 	<div class="modal-body">
									  		<img class="img-responsive img-rounded" src="'.base_url().$row->orig_img_prod.'" alt="'.$row->id_prod.'" />
											<div align="left" class="desc_prod">'.$row->desc_prod.'</div>
										</div>
									</div>
								</div>
							</div>';
					}
				} else {
					echo '<h3>'.$search_not_found.'</h3>';
				}	
			?>
       </div>         
  	</div>
  	<?php include 'right_box.php'; ?>
</div>  
<?php include 'footer.php'; ?>


<script>
	$(document).ready(function () {
		/* Plus-minus cantitatea */
		$(document).on('click', 'input.more', function () {
			valTemp = $(this).siblings('input[type=text]').val();
			valTemp++;
			$(this).siblings('input[type=text]').val(valTemp);
		});
		$(document).on('click', 'input.less', function () {
			valTemp = $(this).siblings('input[type=text]').val();
			valTemp--;
			if (valTemp == 0) {}
			else {
				$(this).siblings('input[type=text]').val(valTemp);
			}
		});
		/* La hover ascunde pretul si afiseaza cantiatea */
		$(document).on('mouseenter', 'article', function () {
			var width = $(window).width();
			if (width > 768) {
				$(this).find('.amt').css("display", "block");
				$(this).find('.price').css("display", "none");
			}
		});
		$(document).on('mouseleave', 'article', function () {
			var width = $(window).width();
			if (width > 768) {
				$(this).find('.amt').css("display", "none");
				$(this).find('.price').css("display", "block");
			}
		});
		/* Trimite la host id + cantitatea si reintoarce noul numarul total de produse */
		$(document).on('click', 'a.buy', function () {
			var id = this.id;
			var contor = $(this).closest("article").find('input[type=text]').val();
			var price = $(this).closest("article").find('.price').text();
			price = parseInt(price);
			$.ajax({
				url: "<?php echo base_url().'user/add_product_to_cart/'?>"
				, type: 'POST'
				, data: {
					id: id
					, count: contor
					, price: price
				}
			}).done(function (result) {
				$(".header-cart").empty();
				$(".header-cart").append('<a href="<?php echo base_url(); ?>user/bag"><span class="cart-title"><i class="fa fa-shopping-cart fa-fw"></i>&nbsp;<?php echo $bag_title; ?></span><span id="total-cart-count" class="my-badge">' + result + '</span></a>');
			});
		});
	});
</script>