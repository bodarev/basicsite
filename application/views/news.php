﻿ <?php include 'lang.php'; ?>
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>

<div class="container">
    <div class="col-lg-8 col-md-8"> 
    	<div class="content"> 	
			<div class="articol" align="center">
            	<?
					foreach ($result as $row)
					{
					echo '<h1>';
					echo $row->title;
					echo '</h1>';
					echo $row->denumire;
					echo '<hr>';
					}
				?>
            </div>    
            <div id="laod_more">
				<form id="myForm">
			  		<input type="hidden" id="limit" value='2' name="limit">
			  		<input type="hidden" id="total" value='<?php echo $total;?>'>
                    <input type="hidden" id="index" value='0' name="index">
				</form>

				<div align="center">
                	<br /> 
                    <a class="paging-more" id="moreLink">   
                        <span class="image">
                            <i id="angle" class="fa fa-angle-down fa-2x"></i>
                        </span>
                        <span><?php echo $LoadMore;?></span>
                    </a>
				</div>
			</div>
  
  		</div>
  	</div>
  	<?php include 'right_box.php'; ?>
    <script>
		function myFunction() {
			var x = Number($('#limit').val());
			var count = Number($('#total').val());
			var input = $( "#limit" );
			$.ajax({
			  url: "<?php echo base_url()."site/load_more/"?>",
			  type: 'POST',
			  data: { valore: x }
			})
			.done(function(result){
				var obj = $.parseJSON(result);
				$.each(obj, function(index, value) {
					$(".col-md-8 .content .articol").append(' <h1>'+value.title+'</h1>'+value.denumire+'<hr>');
				});
			});
			input.val( x + 2);
			var y = Number($('#limit').val());
			if(y >= count ) {
				$('#laod_more').html('');
			}
		}
		
			$(window).scroll(function() {
				if($(window).scrollTop() + $(window).height() == $(document).height()) {
					var z = Number($('#index').val());
					if (z < 2) { 
						myFunction();
						var input = $( "#index" );
						input.val( z + 1);
					} 
				}
			});
			
			
			$('#moreLink').click(function() {
				$('#angle').removeClass().addClass('fa fa-refresh fa-spin fa-2x');
				$('.paging-more ').css('color', '#005580');
				setTimeout(function(){  myFunction(); $('#angle').removeClass().addClass('fa fa-angle-down fa-2x'); $('.paging-more ').css('color', '#bfbfbf'); }, 1500);
				
			})
	</script>
</div>  
<?php include 'footer.php'; ?>