﻿<?php include 'lang.php'; ?>
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>

<div class="container">
    <div class="col-md-12"> <div class="content"> 
  	<?php 
 		echo heading ($contact_title,1);
		echo heading ($contact_desc,4);
	
		$name_errore = form_error('fullName');
		$email_errore = form_error('email');
		$message_errore = form_error('message');
	
		if (!empty($MsgSent)) {
			echo '<div class="alert alert-success">'.$MsgSent.'</div>';
		}
		else { ?>
		
		 <form action="<?php echo base_url()."site/send_email"?>" method="post" accept-charset="utf-8" id="contact">
    
			<div class="row">
				<div class="col-md-7">
                	<span class="my-input-group">
                		<i class="fa fa-user fa-lg"></i> 
						<input type="text" name="fullName" placeholder="Full Name" value="<?php echo $this->input->post('fullName')?>">
					</span>
                </div>
				<div class="col-md-5">
                	<label class="error"><? echo $name_errore; ?></label>
				</div>
            </div>
			
			<div class="row">
				<div class="col-md-7">
                    <span class="my-input-group">
                        <i class="fa fa-envelope-o fa-lg fa-fw"></i> 
                        <input type="text" name="email" placeholder="Email address" value="<?php echo $this->input->post('email')?>">
                    </span>
                </div>
                <div class="col-md-5">    
					<label class="error"><? echo $email_errore; ?></label>
                </div>    
			</div>
			
			<div class="row">
            	<div class="col-md-7">        
                    <span class="my-input-group textarea">
                    	<i class="fa fa-pencil fa-lg fa-fw"></i>
                    	<textarea name="message" placeholder="Message" value="<?php echo $this->input->post('message')?>"> </textarea>
                    </span>
                </div>
                <div class="col-md-5">    
                    <label class="error"><? echo $message_errore; ?></label>
                </div>    
			</div>
			<br />
			 <p><input class="btn btn-success btn-embossed" type="submit" name="login_submit" value="<? echo $ButtonSend; ?>"></p>
    
		</form>
		
<?	}
  ?>
  
  </div>
  </div>

<?php include 'footer.php'; ?>