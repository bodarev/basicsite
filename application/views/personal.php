<?php include 'lang.php'; ?>
<?php include 'header.php'; ?>
<?php include 'navigation.php'; ?>

<div class="container">
    <div class="col-lg-8 col-md-8">
        <div class="content">
           <?php echo heading ($personal_title,1);	
                
             if ($personalData == null ) {
                echo '<br />
                        <p class="alert-msg alert-info">
                            '.$MsgAddAddress.'
                            <i class="fa fa-hand-o-up" aria-hidden="true"></i>
                        </p>';
             } else {
                
				$day = date("j ", $personalData->register_date);
				$month = date("F", $personalData->register_date);
				$year = date(" Y H:i", $personalData->register_date);
				$date = $day.${$month}.$year;
                 
                if ($personalData->avatar > null ) {
                    $avatar = $personalData->avatar;
                } else {
                    $avatar = "prod_img/default.jpg";   
                }
                 
                echo '<br />
				
				<section>
					<article class="personal-data row">
						<div class="col-sm-5 personal-image">
							 <img src="'.base_url().$avatar.'" class="img-thumbnail" width="580" >
                            <div class="upload-image"> 
                                <form role="form" action="" method="post" enctype="multipart/form-data">
								    <input type="file" id="imgInp" name="userfile" class="upload" accept="image/*" />
								    <label for="imgInp"> <span>'.$LinkChangeImage.'&hellip;</span></label>
                                    <input hidden type="submit" name="image_submit" value="test">
                                </form>
								
							</div>
						</div>
						
						<div class="col-sm-7">
							 <p><i class="fa fa-user fa-fw" aria-hidden="true"></i>'.$personalData->name.'
							 	<br />
                                <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>'.$personalData->strada.',&nbsp'
                                .$personalData->bloc;

                                if ($personalData->scara > 0) {	
                                   echo ',&nbsp'.$user_entrance.'&nbsp'
                                  .$personalData->scara;
                                }
                                if ($personalData->etaj > 0) {
                                   echo ',&nbsp'.$user_floor.'&nbsp'
                                  .$personalData->etaj;
                                }
                                if ($personalData->apartament > 0) {
                                   echo ',&nbsp'.$user_apartament_short.'&nbsp'
                                  .$personalData->apartament;
                                }
                                echo '<br />
								<i class="fa fa-home fa-fw" aria-hidden="true"></i>&nbsp'.$personalData->localitate.'
								<br />
								<i class="fa fa-mobile fa-fw" aria-hidden="true"></i>&nbsp'.$user_phone.':&nbsp'.$personalData->phone.'
								<br />
								<i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i>&nbspE-mail:&nbsp'.$personalData->email.'
                                <br />
								<i class="fa fa-calendar fa-fw" aria-hidden="true"></i>&nbsp'.$user_regdate.':&nbsp'.$date.'  
                            </p>
						</div>
					</article>
				</section>'; 
             }
            ?>
        </div>
    </div>
    <?php include 'right_box.php'; ?>
</div>
<?php include 'footer.php'; ?>

<script>
	$(document).ready(function () {
		// prepare instant preview
		$("#imgInp").change(function () {
			$(".upload-image form").trigger("submit");
		});
		$(function () {
			$('.upload-image form').on('submit', function (e) {
				e.preventDefault();
				$.ajax({
					url: "<?php echo base_url().'user/add_avatar_json/'?>"
					, type: 'POST'
					, data: new FormData(this)
					, contentType: false
					, cache: false
					, processData: false
				}).done(function (result) {
					var obj = $.parseJSON(result);
					var avatar = "<?php echo base_url(); ?>" + obj;
					$(".personal-image img").attr("src", avatar);
				});
			});
		});
	});
</script>