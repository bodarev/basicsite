<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	
	public function index()
	{	
		$this->home();
	}
	
	/**************** pages *******************/

	public function unload_data($namePage) {
		$this->load->model("model_get");
		$data = $this->model_get->get_all_data($namePage);
		return $data;
	}
	
	public function home() {
		$data = $this->unload_data('home');
		$this->load->view('home', $data);
	}

	public function about() {
		$data = $this->unload_data('about');
		$this->load->view('about', $data);
	}
	
	public function news() {
		$data = $this->unload_data('news');
		$data['total'] = $this->model_get->get_count_from_news();
		$limit = 0;
		$data['result'] = $this->model_get->get_news($limit);
		$this->load->view('news', $data);	
	}
	
	public function contact() {
		$data = $this->unload_data('contact');
		$data['MsgSent'] = '';
		$this->load->view('contact', $data);	
	}
	
	public function send_email() {
		$data = $this->unload_data('contact');
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('fullName', 'Full Name', 'required|alpha|xss_clean');
		$this->form_validation->set_rules('email', 'Email address', 'required|valid_email|xss_clean');
		$this->form_validation->set_rules('message', 'Message', 'required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');
		
		$this->form_validation->set_message('required', $data['required']);
		$this->form_validation->set_message('alpha', $data['alpha']);
		$this->form_validation->set_message('valid_email', $data['valid_email']);
      
		if($this->form_validation->run() == FALSE){
			$data['MsgSent'] = '';
			$this->load->view('contact', $data);
		}
		else {
			
			$this->load->library('email');
			$this->email->from(set_value("email"), set_value("fullName"));
			$this->email->to("bodarev13@mail.ru");
			$this->email->subject("Message from contacts!");
			$this->email->message(set_value("message"));
			$this->email->send();

			$this->load->view('contact', $data);
		}
		
	}
	
	public function search($match) {
		$match = base64_decode($match);
		$data = $this->unload_data('search');
		if ($match) {
			// scapam de %20 care apar in loc de spatii in input search
			$match = urldecode (  $match );
			$this->load->model("model_get");
			$data['query'] = $this->model_get->get_search($match);
			$data['search'] = $match;
		}
		$this->load->view('search', $data);
	}
	
	/**************** json encode *******************/
	
	public function load_more() {
		$limit = $this->input->post("valore");
		$this->load->model("model_get");
		$data['result'] = $this->model_get->get_news($limit);
		echo json_encode($data['result']);
	}
	
	public function langue($lang='',$link='') {   
        $data = array (
				'langue' => $lang
			);
			$this->session->set_userdata($data);
			
			if($link == 'null') { 
				redirect('/');
			} else {
				$link =  str_replace("-","/", $link);
				redirect($link);
			} 
	}
}