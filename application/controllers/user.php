<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function index() {
		$this->login();
	}
	
	/**************** pages *******************/
	
	public function login($link='') {
		$this->load->model("model_get");
		$data = $this->model_get->get_all_data("login");
		$this->load->view('login', $data);

		$data = array (
			'link-redirect' => $link
		);
		$this->session->set_userdata($data);

	}
		
	public function signup() {
		$this->load->model("model_get");
		$data = $this->model_get->get_all_data("signup");
		$this->load->view('signup', $data);
	}
	
	public function remember() {
		$this->load->model("model_get");
		$data = $this->model_get->get_all_data("remember");
		$this->load->view('remember', $data);
	}
	
	public function personal() {
		if ($this->session->userdata('is_logged_in')) {
			$email = $this->session->userdata('email');
			
			$this->load->model("model_get");
			$data = $this->model_get->get_all_data("personal");
			$data['personalData'] = $this->model_get->get_user_data($email);
			
			$this->load->view('personal', $data);
		}else {
			redirect('user/login');
		}	
	}
	
	public function orders() {
		if ($this->session->userdata('is_logged_in')) {
			$email = $this->session->userdata('email');
			
			$this->load->model("model_get");
			$data = $this->model_get->get_all_data("orders");
		    $data['amount'] = $this->model_get->get_amount();
			$data['result'] = $this->model_get->get_orders($email);		  
			
			$this->load->view('orders', $data);
		}else {
			redirect('user/login');
		}	
	}
    
    public function cancel_order($id) {
		if ($this->session->userdata('is_logged_in')) {
			$email = $this->session->userdata('email');
			
			$this->load->model("model_update");
			$this->model_update->update_order($id,$email);
			redirect('user/orders');
		} else {
			redirect('user/login');
		}	
	}
    
	public function address() {
		if ($this->session->userdata('is_logged_in')) {
			$email = $this->session->userdata('email');
			
			$this->load->model("model_get");
			$data = $this->model_get->get_all_data("address");
			$data['result'] = $this->model_get->get_user_address($email, '');		  
			
			$this->load->view('address', $data);
		}else {
			redirect('user/login');
		}	
	}
	
	public function add_address_user() {
		if ($this->session->userdata('is_logged_in')) {
			$this->load->model("model_get");
			$data = $this->model_get->get_all_data("address");	  
			
			$this->load->view('add-address', $data);
		} else {
			redirect('user/login');
		}	
	}
    
	public function edit_address_user($id) {
		if ($this->session->userdata('is_logged_in')) {
			$email = $this->session->userdata('email');
			
			$this->load->model("model_get");
			$data = $this->model_get->get_all_data("address");
			$data['result'] = $this->model_get->get_user_address($email, $id);		  
			
			$this->load->view('edit-address', $data);
		} else {
			redirect('user/login');
		}	
	}
	
	public function delete_address_user($id) {
		if ($this->session->userdata('is_logged_in')) {
			$email = $this->session->userdata('email');
			
			$this->load->model("model_delete");
			$this->model_delete->delete_user_address($id,$email);
			
			redirect('user/address');
		} else {
			redirect('user/login');
		}	
	}
    
    public function set_default_address_user($id) {
		if ($this->session->userdata('is_logged_in')) {
			$email = $this->session->userdata('email');
			
			$this->load->model("model_update");
			$this->model_update->update_address_user_default($id,$email);
			redirect('user/address');
		} else {
			redirect('user/login');
		}	
	}
    
	public function restricted() {
		$this->load->model("model_get");
		$data = $this->model_get->get_all_data("remember");
		$this->load->view('restricted', $data);
	}
	
    /************ json encode *************/
    
    public function load_order_info() {
		$id_order = $this->input->post("valore");
		$this->load->model("model_get");
		$data['result'] = $this->model_get->get_order_info($id_order);
		echo json_encode($data['result']);
	}
	
	 public function add_avatar(){
		 
		$email = $this->session->userdata('email');
		$this->load->model("model_update");
		$data['avatar'] = $this->model_update->update_avatar($email);
		echo json_encode($data['avatar']);
	}
    
    
     public function add_avatar_json(){
		 
		$email = $this->session->userdata('email');
		$this->load->model("model_update");
		$data['avatar'] = $this->model_update->update_avatar($email);
		echo json_encode($data['avatar']);
	}
    
    /************ login validation *************/
    
	public function login_validation() {
		$this->load->model("model_get");
		$data = $this->model_get->get_all_data("login");
			
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|callback_validate_credentials|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|MD5|xss_clean');
		
		$this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');
		
		$this->form_validation->set_message('required', $data['required']);
		$this->form_validation->set_message('valid_email', $data['valid_email']);
		
		if ($this->form_validation->run()) {
			$data =  array (	
				'email' => $this->input->post('email'),
                'lastEmail' => $this->input->post('email'),
				'is_logged_in' => 1
			);
			$this->session->set_userdata($data);
            
            // recuperam datele 
            $ip = $this->input->ip_address();
            $email = $this->session->userdata('email');
	        
            // adaugam email la produsele care nu au pentru ip curent 
			$this->load->model("model_update");
		    $this->model_update->update_email_from_cart($ip, $email);
            
			if ($this->session->userdata('link-redirect') != '') {
				$link = $this->session->userdata('link-redirect');
				$data = array (
					'link-redirect' => ''
				);
				$this->session->set_userdata($data);
				$link =  str_replace("-","/", $link);
				redirect($link.'/block');
			} else {
				redirect('user/personal');
			}  	  
		} else {
			$this->load->view('login', $data);
		}
	}
    
    public function validate_credentials() {
		$this->load->model("model_get");
		$data = $this->model_get->get_all_data("login");
		
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		if(!empty($email) && !empty($password))
		{
			$this->load->model('model_get');
			
			if($this->model_get->can_log_in()) {
				return true;
			} else {
				$this->form_validation->set_message('validate_credentials', $data['validate_credentials']);
				return false;
			}
		} else {
			$this->form_validation->set_message('validate_credentials', '');
			return false;
		}
	}
	
    /************ signup validation *************/
    
    public function signup_validation() {
		$data = $this->form_validation_signup('click');	
		if ($this->form_validation->run()) {
			//Generate a random key
			
			$key_conferm = md5(uniqid());
			
			$this->load->library('email', array('mailtype' => 'html'));
			
			$this->email->from('info@basicsite.com', 'Bodarev Corporation');
			$this->email->to($this->input->post('email'));
			$this->email->subject("".$data['MsgSubject']."");
			
			$message = "<p> ".$data['MsgMailHeader']."</p>";
			$message .= "<p> <a href='".base_url()."user/register_user/$key_conferm'></a>".$data['LinkConfirmSignup']."</a></p>";
			
			$this->email->message($message);
			
			//Send and email to the user 
			$this->load->model("model_insert");
			if ($this->model_insert->insert_temp_user($key_conferm)) {
				if ($this->email->send()) {
					$data['message'] = $data['MsgEmailSent'];
					$data['tip'] = 'success';
					$this->load->view('alert_msg', $data);
				} else {
					$data['message'] = $data['MsgEmailSentError'];
					$data['tip'] = 'error';
					$this->load->view('alert_msg', $data);
				}
			} else {
				$data['message'] = $data['MsgAddUserError'];
				$data['tip'] = 'error';	
				$this->load->view('alert_msg', $data);
			}
			//Add them to the tem_users db		
			
		} else {
			$this->load->view('signup', $data);
		}
	}
    
	public function form_validation_signup($param) {
		$this->load->model("model_get");
		$data = $this->model_get->get_all_data("signup");
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[users.email]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]|xss_clean');
		$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|matches[password]|min_length[6]|xss_clean');
		
		$this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');
		
		$this->form_validation->set_message('required', $data['required']);
		$this->form_validation->set_message('valid_email', $data['valid_email']);
		$this->form_validation->set_message('is_unique', $data['is_unique']);
		$this->form_validation->set_message('matches', $data['matches']);
		$this->form_validation->set_message('min_length', $data['min_length']);
		
		if ($param == 'param' ) {
			if (!$this->form_validation->run()) {
				$q = array();
				$q['email_errore'] = form_error('email');
				$q['pass_errore'] = form_error('password');
				$q['cpass_errore'] = form_error('cpassword');
				echo json_encode($q);
			}
		}	
		return $data;
	}
    

	/****** inregistrarea unui client nou *******/
    
	public function register_user($key_conferm) {
		$this->load->model("model_get");
		$data = $this->model_get->get_all_data("signup");
		
		if($this->model_get->is_valid_key($key_conferm)) {
			$this->load->model('model_insert');
			if($new_email = $this->model_insert->insert_user($key_conferm)) {
				$sess = array (
					'email' => $new_email,
                    'lastEmail' => $new_email,
					'is_logged_in' => 1
				);
				$this->session->set_userdata($sess);
				redirect('user/orders');
			} else {
				$data['message'] = $data['MsgAddUserError'];
				$data['tip'] = 'error';
				
				$this->load->view('alert_msg', $data);
			}			
		} else {
			$data['message'] = $data['MsgInvalidKey'];
			$data['tip'] = 'error';
	
			$this->load->view('alert_msg', $data);
		}
	}
    
    /********** restabilirea parolei ***********/ 
    
    public function send_password() {
		$data = $this->form_validation_remember('click');	
		if ($this->form_validation->run()) {
			//Generate a random key
			
			$key = 'eraclito';
			$key_new = md5($key);
			
			$this->load->library('email', array('mailtype' => 'html'));
			
			$this->email->from('info@basicsite.com', 'Bodarev Corporation');
			$this->email->to($this->input->post('email'));
			$this->email->subject("".$data['MsgRememberPass']."");
			
			$message = "<p> ".$data['MsgMailHeaderRemember']."</p>";
			$message .= "<br/><p>".$key."</p>";
			
			$this->email->message($message);
			
			//Send and email to the user 
			$this->load->model("model_update");
			if ($this->model_update->update_password($key_new)) {
				if ($this->email->send()) {
					$data['message'] = $data['MsgEmailSent'];
					$data['tip'] = 'success';
					$this->load->view('alert_msg', $data);
				} else {
					$data['message'] = $data['MsgEmailSentError'];
					$data['tip'] = 'error';
					$this->load->view('alert_msg', $data);
				}
			} else {
				$data['message'] = $data['MsgAddUserError'];
				$data['tip'] = 'error';	
				$this->load->view('alert_msg', $data);
			}
			//Add them to the tem_users db		
			
		} else {
			$this->load->view('remember', $data);
		}
	}
    
    public function form_validation_remember($param) {
		$this->load->model("model_get");
		$data = $this->model_get->get_all_data("remember");
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|callback_email_exist|xss_clean');
		
		$this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');
		
		$this->form_validation->set_message('required', $data['required']);
		$this->form_validation->set_message('valid_email', $data['valid_email']);
		$this->form_validation->set_message('email_exist', $data['email_exist']);
		
		if ($param == 'param' ) {
			if (!$this->form_validation->run()) {
				$q = array();
				$q['email_errore'] = form_error('email');
				echo json_encode($q);
			}
		}	
		return $data;
	}
    
	public function email_exist($key){
		$this->load->model("model_get");
		$count = $this->model_get->email_exist($key);
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
	}
    
    /********** validarea adresei ***********/
    
    public function address_validation($id, $param) {
		$email = $this->session->userdata('email');
		
		$this->load->model("model_get");
		$data = $this->model_get->get_all_data("address");
			
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', $data['user_name'], 'required|xss_clean');
		$this->form_validation->set_rules('city', $data['user_city'], 'required|xss_clean');
		$this->form_validation->set_rules('strada', $data['user_strada'], 'required|xss_clean');
		$this->form_validation->set_rules('bloc', $data['user_bloc'], 'required|xss_clean');
		$this->form_validation->set_rules('phone', $data['user_phone'], 'required|callback_regex_check|xss_clean');
		
		$this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');
		
		$this->form_validation->set_message('required', $data['required']);
		$this->form_validation->set_message('numeric', $data['numeric']);
		$this->form_validation->set_message('min_length', $data['min_length']);
		$this->form_validation->set_message('max_length', $data['max_length']);
        $this->form_validation->set_message('regex_check', 'test - oricum nu se afiseaza.');
		
		if ($this->form_validation->run()) {
			$array = array(
			   'email'      => $this->session->userdata('email'),
               'name'       => $this->input->post("name"),
               'localitate' => $this->input->post("city"),
               'strada'     => $this->input->post("strada"),
			   'bloc'       => $this->input->post("bloc"),
			   'scara'      => $this->input->post("scara"),
			   'etaj'       => $this->input->post("etaj"),
			   'apartament' => $this->input->post("apartament"),
			   'interfon'   => $this->input->post("interfon"),
			   'phone'      => $this->input->post("phone")
            );
			if($id == 'add') {
				$this->load->model("model_insert");
				$this->model_insert->insert_user_address($array);
				redirect('user/address');
			} else {
				$this->load->model("model_update");
				$this->model_update->update_address_user($id,$email,$array);
				redirect('user/address');
			}
		} else {
			if ($param == 'dinamic') {
				$q = array();
				$q['name_errore'] = form_error('name');
				$q['city_errore'] = form_error('city');
				$q['strada_errore'] = form_error('strada');
				$q['bloc_errore'] = form_error('bloc');
				$q['phone_errore'] = form_error('phone');
				echo json_encode($q);
			} else {
				if($id == 'add') {		  
					$this->load->view('add-address', $data);
				} else {
					$this->load->model("model_get");
					$data['result'] = $this->model_get->get_user_address($email, $id);		  
					$this->load->view('edit-address', $data);
				}
			}
		}
	}
	
    public function regex_check($key){
        if (1 !== preg_match("/^\+373(\s+)?\(?(79|78|69|68|67|60)\)?(\s+)?[0-9]{3}-?[0-9]{3}$/", $key))
        {
            return false;
        }
        else
        {   
            return true;
        }
	}
    
    /********** trimite comanda ***********/
    
     public function add_order() {
        $this->load->model("model_get");
		$data = $this->model_get->get_all_data("orders"); 
         
        $option1 = $this->input->post("option1");
		$option2 = $this->input->post("option2");
		$option3 = $this->input->post("option3");
		$option4 = $this->input->post("option4");   
         
        if (($option1 == 'on' or $option2 == 'on') && 
            ($option3 == 'on' or $option4 == 'on')) { 
          
            if ($option1 == 'on') {
                $pay_method = '1';    
            } 
            if ($option2 == 'on') {
                $pay_method = '2';    
            }
            if ($option3 == 'on') {
                $reception = '1';    
            } 
            if ($option4 == 'on') {
                $reception = '2';    
            } 

            if ($reception == '1') {

                $this->load->library('form_validation');
                $this->form_validation->set_rules('AddressClient', 'Address Client', 'required|callback_cexist|xss_clean');

                $this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');

                $this->form_validation->set_message('required', $data['required']);
                $this->form_validation->set_message('client_exist', 'client test - oricum nu se afiseaza'); 

             } else {

                $this->load->library('form_validation');
                $this->form_validation->set_rules('AddressRestaurant', 'Address Restaurant', 'required|callback_rexist|xss_clean');

                $this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');

                $this->form_validation->set_message('required', $data['required']);
                $this->form_validation->set_message('restaurant_exist', 'restaurant test - oricum nu se afiseaza');

             }

            if ($this->form_validation->run()) {

                $ip = $this->input->ip_address();
                $email = $this->session->userdata('lastEmail');

                if ($reception == '1') {
                    $email = $this->session->userdata('email');
                    $id_address = $this->input->post("AddressClient");
                    $x = $this->model_get->get_user_address($email, $id_address);
                    $array = $x[0];
                    $address = $array->localitate.', '.$array->strada.' '.$array->bloc;
                } else {
                    $id_address = $this->input->post("AddressRestaurant");
                    $x = $this->model_get->get_restaurant_address($id_address);
                    $array = $x[0];
                    $address = $array->localitate.', '.$array->strada.' '.$array->bloc;
                }
                
                $date = time();
                $random = random_string('numeric', 6);

                $in_code = $date.$random; 

                $this->load->model("model_functions"); 
                $out_code = $this->model_functions->alphaID($in_code);

                // calculam totalul 

                $total_prod = 0;
                $total = 0;

                $result = $this->model_get->get_products(0, $ip, $email);

                foreach ($result as $row) {

                    $array = array( 
                       'id_order'    => $out_code,
                       'id_prod'     => $row->id_prod,
                       'quantity'    => $row->quantity,
                       'price'       => $row->pret_prod
                    );

                    $total_prod = $row->quantity * $row->pret_prod;
                    $total = $total + $total_prod;

                    $this->load->model("model_insert"); 
                    $this->model_insert->insert_order($array);  
                }

                // verificam daca plateste livrarea

                $amount = $this->model_get->get_amount();

                if ($amount['freeDelivery'] > $total && $reception == '1') {
                    $delivery = $amount['delivery'];
                } else {
                    $delivery = 0;
                }
                
                // scoatem numele utilizatorului
                
                $datauser = $this->model_get->get_user_data($email);
                $name = $datauser->name;
                $phone = $datauser->phone;

                $arrayTotal = array(
                   'email'       => $this->session->userdata('email'),
                   'name'        => $name,
                   'phone'       => $phone,
                   'id_order'    => $out_code,
                   'pay_method'  => $pay_method,
                   'reception'   => $reception,
                   'address'     => $address,
                   'total'       => $total,
                   'delivery'    => $delivery,
                   'status'      => 'waiting',
                   'date'        => $date
                );

                $this->load->model("model_insert"); 
                $this->model_insert->insert_total_order($arrayTotal);  
                
                redirect ('user/orders');
        
    /* Nu este valorizata adresa indicata */
            } else {

                $this->load->model("model_get");
                $data = $this->model_get->get_all_data("bag");

                $ip = $this->input->ip_address();
                $email = $this->session->userdata('lastEmail');

                $data['result'] = $this->model_get->get_products(0, $ip, $email);
                $data['amount'] = $this->model_get->get_amount();
                $data['user_address'] = $this->model_get->get_user_address($email,'');
                $data['restaurant_address'] = $this->model_get->get_restaurant_address();

                // pentru a avea modal dialog activat

                $data['display'] = 'block';

                // verificam care input e checked pentru al activa in pagina

                if ($option1 == 'on') {
                    $data['radio1'] = '1';     
                } 
                if ($option2 == 'on') {
                    $data['radio1'] = '2';    
                }
                if ($option3 == 'on') {
                    $data['radio2'] = '1';    
                } 
                if ($option4 == 'on') {
                    $data['radio2'] = '2'; 
                } 

                $this->load->view('products', $data);
            }
            
    /* Nu sunt apasate butoanele de plata si preluare  */      
        } else {  
            
            $this->load->model("model_get");
            $data = $this->model_get->get_all_data("bag");

            $ip = $this->input->ip_address();
            $email = $this->session->userdata('lastEmail');

            $data['result'] = $this->model_get->get_products(0, $ip, $email);
            $data['amount'] = $this->model_get->get_amount();
            $data['user_address'] = $this->model_get->get_user_address($email,'');
            $data['restaurant_address'] = $this->model_get->get_restaurant_address();

            // pentru a avea modal dialog activat

            $data['display'] = 'block';
            
            // apasam default buton 1 si 1 
            
            $data['radio1'] = '1';
            $data['radio2'] = '1';
            
            $this->load->view('products', $data);
        
        }
    }

    public function cexist($key){
		$this->load->model("model_get");
		$count = $this->model_get->client_exist($key);
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
	}
    
    public function rexist($key){
		$this->load->model("model_get");
		$count = $this->model_get->restaurant_exist($key);
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
	}  
    
     /*************** logout ****************/
    
    public function logout() {
        
        $this->session->unset_userdata('is_logged_in');
        $this->session->unset_userdata('email');
        
		redirect('user/login');
	}
    
    /**************** pages *******************/

	public function bag($param='none') {
		$ip = $this->input->ip_address();
		$email = $this->session->userdata('lastEmail');
		
	    $this->load->model("model_get");
		$data = $this->model_get->get_all_data("bag");
		$data['result'] = $this->model_get->get_products(0, $ip, $email);
		$data['amount'] = $this->model_get->get_amount();
		$data['user_address'] = $this->model_get->get_user_address($email,'');
        $data['restaurant_address'] = $this->model_get->get_restaurant_address();
		$data['display'] = $param;
        $data['radio1'] = '1';
        $data['radio2'] = '1'; 
		$this->load->view('bag', $data);	
	}

	public function menu() {
	    $this->load->model("model_get");
		$data = $this->model_get->get_all_data("menu");
		$data['result'] = $this->model_get->get_all_products(0);
        $data['category'] = $this->model_get->get_category();
		$this->load->view('menu', $data);	
	}

/**************** json encode *******************/	
	
	public function load_personal_menu() {
		$categ = $this->input->post("valore");
		$this->load->model("model_get");
		$data['result'] = $this->model_get->get_all_products($categ);
		echo json_encode($data['result']);
	}
    
	public function remove_product_from_cart() {
	// scotem variabilele necesare
		$id = $this->input->post("id");
		$ip = $this->input->ip_address();
        $email = $this->session->userdata('lastEmail');
 
	// stergem produsul curent
		$this->load->model("model_delete");
		$this->model_delete->delete_product_from_cart($id, $ip, $email);
	
	// selectam datele pentru ip curent	
		$this->load->model("model_get");
		$data['result'] = $this->model_get->get_products(0, $ip, $email);
		echo json_encode($data['result']);	
	}
	
	public function get_quantity_from_cart() {
	// scotem variabilele necesare	
		$ip = $this->input->ip_address();
        $email = $this->session->userdata('lastEmail'); 
        
	//selecta datele pentru ip curent
		$this->load->model("model_get");	
		$result = $this->model_get->get_count_from_cart($ip, $email);
		if ( $result > 0 ) {
			echo $result;
		} else { 
			echo '0'; 
		}			
	}
	
	public function add_product_to_cart() {
	// scotem variabilele necesare
		$id = $this->input->post("id");
		$count = $this->input->post("count");
		$price = $this->input->post("price");
		$ip = $this->input->ip_address();
        $email = $this->session->userdata('lastEmail');
		
	// verificam daca mai  exista date cu asa id + ip
		$this->load->model("model_get");
		$quantity = $this->model_get->get_count_product_from_cart($id, $ip, $email);	
		if ($quantity > 0 ) 
		{
			$this->load->model("model_delete");
			$this->model_delete->delete_product_from_cart($id, $ip, $email);	
		}
		$data = array(
		   'id_prod' => $id ,
		   'quantity' => $count,
		   'price' => $price,
		   'id_client' => $ip,
           'email' => $email
		);
		
	// inseram datele
		$this->load->model("model_insert"); 
		$this->model_insert->insert_product_to_cart($data);
		
	//selecta datele pentru ip curent
		$this->load->model("model_get");
		$result = $this->model_get->get_count_from_cart($ip, $email);
		if ( $result > 0 ) {
			echo $result;
		} else { 
			echo '0'; 
		}	
	}
}