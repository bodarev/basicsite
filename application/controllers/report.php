<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

    public function __construct()
    {
        parent::__construct();    
        $this->load->library('pdf_report');    
    }
    

    public function index()
    {
        if ($this->session->userdata('is_logged_in')) {
			$email = $this->session->userdata('email');
			
            $this->load->model("model_get");
			$data = $this->model_get->get_all_data("orders");
		    $data['amount'] = $this->model_get->get_amount();
			$data['result'] = $this->model_get->get_orders($email);		  
			
			$this->load->view('pdfreport', $data);
		}else {
			redirect('user/login');
		}	
        
    }

}

/* End of file Report.php */
