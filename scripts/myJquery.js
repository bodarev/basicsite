// principal
$(document).ready(function () {
	
	// Deschide/inchide meniul cu categorii dropdown
	$(document).on('click', '.dropbtn', function (e) {
		if ( $( this ).parent().hasClass( "open" )) {
			$('.dropbtn').css({
				"border-bottom-left-radius": "0px"
				, "border-bottom-right-radius": "0px"
				, "border-bottom-color": "#5d9d50"
				, "box-shadow": "0px 8px 16px 0px rgba(0,0,0,0.2)"
				, "webkit-box-shadow": "0px 8px 16px 0px rgba(0,0,0,0.2)"
			});
			$('.dropbtn i').removeClass('fa-chevron-down').addClass('fa-thumb-tack');
			e.stopPropagation();
		}
		else {
			$('.dropbtn').css({
				"border-bottom-left-radius": "5px"
				, "border-bottom-right-radius": "5px"
				, "box-shadow": "inset 0 -2px 0 rgba(0, 0, 0, .15)"
				, "webkit-box-shadow": "inset 0 -2px 0 rgba(0, 0, 0, .15)"
			});
			$('.dropbtn i').removeClass('fa-thumb-tack').addClass('fa-chevron-down');
		}
	});
	// Inchide meniul cu categorii cand dai click pe orce in afara
	$(document).click(function () {
		$('.dropbtn').css({
			"border-bottom-left-radius": "5px"
			, "border-bottom-right-radius": "5px"
			, "box-shadow": "inset 0 -2px 0 rgba(0, 0, 0, .15)"
			, "webkit-box-shadow": "inset 0 -2px 0 rgba(0, 0, 0, .15)"
		});
		$('.dropbtn i').removeClass('fa-thumb-tack').addClass('fa-chevron-down');
	});
	// sterge mesajul de eroare la click
	$(document).on("click", ".fa-close", function () {
		$(this).parent().css("display", "none");
	});
	// dinamic validate register
	$("#email").on("focus", function () {
		if ($("#email").val().length == 0) {
			$("#email-group .fa-envelope-o").css("color", "#a8c9e4");
		}
	});
	$("#password").on("focus", function () {
		if ($("#password").val().length == 0) {
			$("#password-group .fa-lock").css("color", "#a8c9e4");
		}
	});
	$("#cpassword").on("focus", function () {
		if ($("#cpassword").val().length == 0) {
			$("#cpassword-group .fa-lock").css("color", "#a8c9e4");
		}
	});
	$("#register").on("keyup", function () {
		var contor = 0;
		var em = $("#email").val();
		var str = $("#password").val();
		var cstr = $("#cpassword").val();
		var regexp1 = /[A-Z]/g;
		var regexp2 = /[a-z]/g;
		var regexp3 = /[1-9]/g;
		var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
		// icon password color
		if (em.length > 0) {
			if (pattern.test(em)) {
				$("#email-group .fa-envelope-o").css("color", "#5D9D50");
			}
			else {
				$("#email-group .fa-envelope-o").css("color", "#ce4844");
			}
		}
		else {
			if ($("#email").is(":focus")) {
				$("#email-group .fa-envelope-o").css("color", "#a8c9e4");
			}
			else {
				$("#email-group .fa-envelope-o").css("color", "#777777");
			}
		}
		if (str.length > 5) {
			contor++;
			$("#cpassword").prop("disabled", false);
		}
		else {
			$("#cpassword").prop("disabled", true);
		}
		if (str.match(regexp1)) {
			contor++;
		}
		if (str.match(regexp2)) {
			contor++;
		}
		if (str.match(regexp3)) {
			contor++;
		}
		// icon password color
		switch (contor) {
		case 4:
			$("#password-group .fa-lock").css("color", "#5D9D50");
			break;
		case 3:
			$("#password-group .fa-lock").css("color", "#cc8c1c");
			break;
		case 2:
			$("#password-group .fa-lock").css("color", "#cc601c");
			break;
		case 1:
			$("#password-group .fa-lock").css("color", "#ce4844");
			break;
		default:
			if ($("#password").is(":focus")) {
				$("#password-group .fa-lock").css("color", "#a8c9e4");
			}
			else {
				$("#password-group .fa-lock").css("color", "#777777");
			}
		}
		// icon confirm password color	
		if (cstr.length > 0) {
			if (str == cstr) {
				$("#cpassword-group .fa-lock").css("color", "#5D9D50");
			}
			else {
				$("#cpassword-group .fa-lock").css("color", "#ce4844");
			}
		}
		else {
			if ($("#cpassword").is(":focus")) {
				$("#cpassword-group .fa-lock").css("color", "#a8c9e4");
			}
			else {
				$("#cpassword-group .fa-lock").css("color", "#777777");
			}
		}
	})
	$("#email").on("blur", function () {
		if ($("#email").val().length == 0) {
			$("#email-group .fa-envelope-o").css("color", "#777777");
		}
	});
	$("#password").on("blur", function () {
		if ($("#password").val().length == 0) {
			$("#password-group .fa-lock").css("color", "#777777");
		}
	});
	$("#cpassword").on("blur", function () {
		if ($("#cpassword").val().length == 0) {
			$("#cpassword-group .fa-lock").css("color", "#777777");
		}
	});
	// Transforma din menu in close 
	$(function () {
		var touch = $('#touch-menu');
		$(touch).click(function () {
			$('.mobile-navigation').toggleClass('visible');
		})
	});
	// Menu resize
	var touch = $('#touch-menu');
	var menu = $('.navigation');
	$(touch).on('click', function (e) {
		e.preventDefault();
		menu.slideToggle();
	});
	$(window).resize(function () {
		var wid = $(window).width();
		if (wid > 768 && menu.is(':hidden')) {
			menu.removeAttr('style');
		}
	});
	// Floating navigation menu
	$(function () {
		var nav = $('#float');
		var navHomeY = nav.offset().top;
		var isFixed = false;
		var $w = $(window);
		$w.scroll(function () {
			var scrollTop = $w.scrollTop();
			var shouldBeFixed = scrollTop > navHomeY;
			if (shouldBeFixed && !isFixed) {
				nav.css({
					position: 'fixed'
					, top: 0
					, width: '100%'
				});
				isFixed = true;
			}
			else if (!shouldBeFixed && isFixed) {
				nav.css({
					position: 'static'
				});
				isFixed = false;
			}
		});
		// scroll top 
		if ($('#back-to-top').length) {
			var scrollTrigger = 100, // px
				backToTop = function () {
					var scrollTop = $(window).scrollTop();
					if (scrollTop > scrollTrigger) {
						$('#back-to-top').addClass('show');
					}
					else {
						$('#back-to-top').removeClass('show');
					}
				};
			backToTop();
			$(window).on('scroll', function () {
				backToTop();
			});
			$('#back-to-top').on('click', function (e) {
				e.preventDefault();
				$('html,body').animate({
					scrollTop: 0
				}, 700);
			});
		}
	});
	// afiseaza/ascunde textul : schimba imaginea 
	$(document).on('mouseover', 'article.personal-data .personal-image', function () {
		$('.upload-image').show();
	});
	$(document).on('mouseout', 'article.personal-data .personal-image', function () {
		$('.upload-image').hide();
	});
	// afiseaza/ascunde adresa client/restaurant 
	$("#orderDelivery").on('click', function () {
		$("#orderAddressClient").show("slow");
		$("#orderAddressRestaurant").hide("slow");
	});
	$("#orderFromRestaurant").on('click', function () {
		$("#orderAddressClient").hide("slow");
		$("#orderAddressRestaurant").show("slow");
	});
	// Add slideDown animation to Bootstrap dropdown when expanding.
	$('.dropdown').on('show.bs.dropdown', function () {
		$(this).find('.dropdown-menu').first().stop(true, true).slideDown();
	});
	// Add slideUp animation to Bootstrap dropdown when collapsing.
	$('.dropdown').on('hide.bs.dropdown', function () {
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp();
	});
	
	// Inchide area select
	$(document).on('click', 'article #cancel-areaselect', function () {
		$('#uploadPreview').css({
			"display": "none"
		});
		$('.imgareaselect-outer').css({
			"display":"none"
		});
		$('.imgareaselect-selection').parent().css({
			"display":"none"
		});
	});
	
	$(document).on('click', 'article #yes-areaselect', function () {
		
		$principal = $('.imgareaselect-selection').parent();
		alert($principal.length);
		if ( $principal.css('display') == 'none' ) {
			alert(123);
		}
	});
	
	
});
// afiseaza in input formatul telefonului 
window.onload = function () {
	MaskedInput({
		elm: document.getElementById('phone'), // select only by id
		format: '+373 (__) ___-___'
		, separator: '+373 ()-'
	});
};
// sterge mesajul defaul required
document.addEventListener('invalid', (function () {
	return function (e) {
		e.preventDefault();
		document.getElementById('phone').focus();
	};
})(), true);